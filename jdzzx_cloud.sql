/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : jdzzx_cloud

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 18/02/2022 22:53:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bus_account
-- ----------------------------
DROP TABLE IF EXISTS `bus_account`;
CREATE TABLE `bus_account`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `balance` int NULL DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bus_account
-- ----------------------------
INSERT INTO `bus_account` VALUES (1, '李四', 2000);
INSERT INTO `bus_account` VALUES (3, '王五', 4000);

-- ----------------------------
-- Table structure for business_message_user
-- ----------------------------
DROP TABLE IF EXISTS `business_message_user`;
CREATE TABLE `business_message_user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '消息发送id',
  `message_id` int NULL DEFAULT NULL COMMENT '消息id',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `redonly_status` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '消息状态(1.未读。2.已读)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '发送时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '消息用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_message_user
-- ----------------------------
INSERT INTO `business_message_user` VALUES (10, 10, 100, '2', '2022-01-12 22:46:38', '2022-01-13 22:50:41');
INSERT INTO `business_message_user` VALUES (11, 10, 101, '2', '2022-01-12 22:46:38', '2022-01-13 22:54:49');
INSERT INTO `business_message_user` VALUES (12, 10, 100, '1', '2022-01-13 22:52:22', NULL);

-- ----------------------------
-- Table structure for business_notice_message
-- ----------------------------
DROP TABLE IF EXISTS `business_notice_message`;
CREATE TABLE `business_notice_message`  (
  `message_id` int NOT NULL AUTO_INCREMENT COMMENT '消息ID',
  `message_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息标题',
  `message_content` longblob NULL COMMENT '消息内容',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `message_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息状态（1.未发送，2.已发送）',
  PRIMARY KEY (`message_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_notice_message
-- ----------------------------
INSERT INTO `business_notice_message` VALUES (10, '系统消息：您的版权申请已经通过了', 0xE682A8E79A84E78988E69D83E794B3E8AFB7E5B7B2E7BB8FE9809AE8BF87E4BA86313231323231, '2022-01-12 22:08:57', '厉害了', '2');

-- ----------------------------
-- Table structure for business_order
-- ----------------------------
DROP TABLE IF EXISTS `business_order`;
CREATE TABLE `business_order`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品类型（1.作品，2.图片，3.软件）',
  `product_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品图片',
  `product_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品编号',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品名称',
  `product_right` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '版权权利（（复制权，发行权，出租权，展览权，表演权，广播权，其他权利）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `auth_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '授权方式',
  `product_money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '版权价格',
  `product_id` int NULL DEFAULT NULL COMMENT '产品id',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单编号',
  `order_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单状态（1，待支付，2.已完成，3.已取消）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_order
-- ----------------------------
INSERT INTO `business_order` VALUES (1, '2', 'http://127.0.0.1:9000/files/product/b82073d886e04033aec88b29c7e73e24.jpg', 'CP20220111110555', '素描大师的画像', '1,2,4', '2022-01-11 16:56:45', '普通授权', '100', 10, 1, 'DD20220111165645', '2');
INSERT INTO `business_order` VALUES (4, '1', 'http://127.0.0.1:9000/files/product/ed0e9737151c42c4af6f96e643006b3a.jpg', 'CP20220110225736', '安徒生童话', '1,2,3,4', '2022-01-11 21:14:44', '普通授权', '100', 9, 1, 'DD20220111211443', '3');
INSERT INTO `business_order` VALUES (5, '1', 'http://127.0.0.1:9000/files/product/ed0e9737151c42c4af6f96e643006b3a.jpg', 'CP20220110225736', '安徒生童话', '1,2,3,4', '2022-01-11 22:01:47', '普通授权', '100', 9, 100, 'DD20220111220147', '2');
INSERT INTO `business_order` VALUES (6, '2', 'http://127.0.0.1:9000/files/product/b82073d886e04033aec88b29c7e73e24.jpg', 'CP20220111110555', '素描大师的画像', '1,2,4', '2022-01-11 22:01:53', '普通授权', '100', 10, 100, 'DD20220111220152', '2');
INSERT INTO `business_order` VALUES (7, '1', 'http://127.0.0.1:9000/files/product/ed0e9737151c42c4af6f96e643006b3a.jpg', 'CP20220110225736', '安徒生童话', '1,2,3,4', '2022-01-12 15:25:44', '普通授权', '100', 9, 1, 'DD20220112152544', '1');
INSERT INTO `business_order` VALUES (8, '1', 'http://127.0.0.1:9000/files/product/ed0e9737151c42c4af6f96e643006b3a.jpg', 'CP20220110225736', '安徒生童话', '1,2,3,4', '2022-01-13 22:51:10', '普通授权', '100', 9, 100, 'DD20220113225109', '2');

-- ----------------------------
-- Table structure for business_product
-- ----------------------------
DROP TABLE IF EXISTS `business_product`;
CREATE TABLE `business_product`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品类型（1.作品，2.图片，3.软件）',
  `product_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品图片',
  `product_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品编号',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '产品名称',
  `product_right` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '版权权利（1.复制权，2.发行权，3.出租权，4.展览权，5.表演权，6.广播权，7.其他权利）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `auth_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '授权方式',
  `product_money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '版权价格',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `apply_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '申请状态（1.草稿，2，待审核，3，不通过，4.已发布，5.下架）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '产品申请表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_product
-- ----------------------------
INSERT INTO `business_product` VALUES (9, '1', 'http://127.0.0.1:9000/files/product/ed0e9737151c42c4af6f96e643006b3a.jpg', 'CP20220110225736', '安徒生童话', '1,2,3,4', '2022-01-10 22:51:46', '普通授权', '100', 1, '4');
INSERT INTO `business_product` VALUES (10, '2', 'http://127.0.0.1:9000/files/product/b82073d886e04033aec88b29c7e73e24.jpg', 'CP20220111110555', '素描大师的画像', '1,2,4', '2022-01-11 11:05:56', '普通授权', '100', 1, '4');
INSERT INTO `business_product` VALUES (17, '1', 'http://127.0.0.1:9000/files/product/76e8d320c38345e1978b59f3aad52b09.png', 'CP20220111213740', '作品1', '1,2', '2022-01-11 21:37:42', '普通授权', '12', 1, '4');
INSERT INTO `business_product` VALUES (18, '3', 'http://127.0.0.1:9000/files/product/915713642d6d4294961428e5124ee292.png', 'CP20220111215722', 'java', '1,3,4', '2022-01-11 21:57:23', 'vip授权', '200', 100, '4');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (3, 'business_product', '产品表', NULL, NULL, 'BusinessProduct', 'crud', 'com.jdzzx.system', 'system', 'product', '产品', 'zzx', '0', '/', '{\"parentMenuId\":\"2007\"}', 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34', NULL);
INSERT INTO `gen_table` VALUES (4, 'business_order', '订单表', NULL, NULL, 'BusinessOrder', 'crud', 'com.jdzzx.system', 'system', 'order', '订单', 'zzx', '1', 'E:\\Generator', '{\"parentMenuId\":\"2020\"}', 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03', NULL);
INSERT INTO `gen_table` VALUES (5, 'business_notice_message', '消息表', NULL, NULL, 'BusinessNoticeMessage', 'crud', 'com.jdzzx.system', 'system', 'message', '消息发布', 'zzx', '1', 'E:\\Generator', '{\"parentMenuId\":\"2044\"}', 'admin', '2022-01-12 21:37:42', '', '2022-01-12 22:04:33', NULL);
INSERT INTO `gen_table` VALUES (7, 'business_message_user', '消息用户表', NULL, NULL, 'BusinessMessageUser', 'crud', 'com.jdzzx.system', 'system', 'user', '消息用户', 'zzx', '0', '/', NULL, 'admin', '2022-01-12 22:43:30', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (8, 'bus_account', '业务账号表', NULL, NULL, 'BusAccount', 'crud', 'com.jdzzx.system', 'system', 'account', '账单表', 'zzx', '0', '/', '{}', 'admin', '2022-02-18 16:56:03', '', '2022-02-18 16:57:23', '测试分布式事务');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (22, '3', 'id', '主键', 'int', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (23, '3', 'product_type', '产品类型（1.作品，2.图片，3.软件）', 'varchar(255)', 'String', 'productType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (24, '3', 'product_logo', '产品图片', 'varchar(255)', 'String', 'productLogo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (25, '3', 'product_no', '产品编号', 'varchar(255)', 'String', 'productNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (26, '3', 'product_name', '产品名称', 'varchar(255)', 'String', 'productName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (27, '3', 'product_right', '版权权利（1.复制权，2.发行权，3.出租权，4.展览权，5.表演权，6.广播权，7.其他权利）', 'varchar(255)', 'String', 'productRight', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (28, '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (29, '3', 'auth_method', '授权方式', 'varchar(255)', 'String', 'authMethod', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (30, '3', 'product_money', '版权价格', 'varchar(255)', 'String', 'productMoney', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-01-10 21:48:58', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (31, '3', 'user_id', '用户id', 'int', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, '', '2022-01-10 21:49:49', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (32, '3', 'apply_status', '申请状态（1.草稿，2，通过，3，不通过）', 'varchar(255)', 'String', 'applyStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 11, '', '2022-01-10 21:49:49', '', '2022-01-10 22:14:34');
INSERT INTO `gen_table_column` VALUES (33, '4', 'id', '主键', 'int', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (34, '4', 'product_type', '产品类型（1.作品，2.图片，3.软件）', 'varchar(255)', 'String', 'productType', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'product_type', 2, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (35, '4', 'product_logo', '产品图片', 'varchar(255)', 'String', 'productLogo', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (36, '4', 'product_no', '产品编号', 'varchar(255)', 'String', 'productNo', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (37, '4', 'product_name', '产品名称', 'varchar(255)', 'String', 'productName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (38, '4', 'product_right', '版权权利（（复制权，发行权，出租权，展览权，表演权，广播权，其他权利）', 'varchar(255)', 'String', 'productRight', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'product_right', 6, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (39, '4', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (40, '4', 'auth_method', '授权方式', 'varchar(255)', 'String', 'authMethod', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 8, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (41, '4', 'product_money', '版权价格', 'varchar(255)', 'String', 'productMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (42, '4', 'product_id', '产品id', 'int', 'Long', 'productId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (43, '4', 'user_id', '用户id', 'int', 'Long', 'userId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (44, '4', 'order_no', '订单编号', 'varchar(255)', 'String', 'orderNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (45, '4', 'order_status', '订单状态（1，待支付，2.已完成）', 'varchar(255)', 'String', 'orderStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'order_status', 13, 'admin', '2022-01-11 16:13:53', '', '2022-01-11 17:16:03');
INSERT INTO `gen_table_column` VALUES (46, '5', 'message_id', '消息ID', 'int', 'Long', 'messageId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-12 21:37:42', '', '2022-01-12 22:04:33');
INSERT INTO `gen_table_column` VALUES (47, '5', 'message_title', '消息标题', 'varchar(50)', 'String', 'messageTitle', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-01-12 21:37:42', '', '2022-01-12 22:04:33');
INSERT INTO `gen_table_column` VALUES (48, '5', 'message_content', '消息内容', 'longblob', 'String', 'messageContent', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'editor', '', 3, 'admin', '2022-01-12 21:37:42', '', '2022-01-12 22:04:33');
INSERT INTO `gen_table_column` VALUES (49, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'datetime', '', 4, 'admin', '2022-01-12 21:37:42', '', '2022-01-12 22:04:33');
INSERT INTO `gen_table_column` VALUES (50, '5', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-01-12 21:37:42', '', '2022-01-12 22:04:33');
INSERT INTO `gen_table_column` VALUES (51, '5', 'message_status', '消息状态（1.未发送，2.已发送）', 'varchar(255)', 'String', 'messageStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'message_status', 6, '', '2022-01-12 21:47:01', '', '2022-01-12 22:04:33');
INSERT INTO `gen_table_column` VALUES (58, '7', 'id', '消息发送id', 'int', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-12 22:43:30', '', NULL);
INSERT INTO `gen_table_column` VALUES (59, '7', 'message_id', '消息id', 'int', 'Long', 'messageId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-01-12 22:43:30', '', NULL);
INSERT INTO `gen_table_column` VALUES (60, '7', 'user_id', '用户id', 'int', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-01-12 22:43:30', '', NULL);
INSERT INTO `gen_table_column` VALUES (61, '7', 'redonly_status', '消息状态(1.未读。2.已读)', 'char(10)', 'String', 'redonlyStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 4, 'admin', '2022-01-12 22:43:30', '', NULL);
INSERT INTO `gen_table_column` VALUES (62, '7', 'create_time', '发送时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2022-01-12 22:43:30', '', NULL);
INSERT INTO `gen_table_column` VALUES (63, '7', 'update_time', '阅读时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-01-12 22:43:30', '', NULL);
INSERT INTO `gen_table_column` VALUES (64, '8', 'id', '主键', 'int', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-02-18 16:56:03', '', '2022-02-18 16:57:23');
INSERT INTO `gen_table_column` VALUES (65, '8', 'name', '姓名', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-02-18 16:56:03', '', '2022-02-18 16:57:23');
INSERT INTO `gen_table_column` VALUES (66, '8', 'balance', '金额', 'int', 'Long', 'balance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-02-18 16:56:03', '', '2022-02-18 16:57:23');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `fired_time` bigint NOT NULL,
  `sched_time` bigint NOT NULL,
  `priority` int NOT NULL,
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `last_checkin_time` bigint NOT NULL,
  `checkin_interval` bigint NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `repeat_count` bigint NOT NULL,
  `repeat_interval` bigint NOT NULL,
  `times_triggered` bigint NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `int_prop_1` int NULL DEFAULT NULL,
  `int_prop_2` int NULL DEFAULT NULL,
  `long_prop_1` bigint NULL DEFAULT NULL,
  `long_prop_2` bigint NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `next_fire_time` bigint NULL DEFAULT NULL,
  `prev_fire_time` bigint NULL DEFAULT NULL,
  `priority` int NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `start_time` bigint NOT NULL,
  `end_time` bigint NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `misfire_instr` smallint NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-01-04 14:51:54', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-01-04 14:51:54', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-01-04 14:51:54', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2022-01-04 14:51:54', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 125 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '新闻', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-01-04 14:51:54', 'admin', '2022-01-12 10:36:45', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 1, '作品', '1', 'product_type', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:45:02', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 2, '图片', '2', 'product_type', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:45:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (102, 3, '软件', '3', 'product_type', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:45:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 1, '复制权', '1', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:46:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 2, '发行权', '2', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:46:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 3, '出租权', '3', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:47:07', 'admin', '2022-01-10 15:47:14', NULL);
INSERT INTO `sys_dict_data` VALUES (106, 4, '展览权', '4', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:47:31', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 5, '表演权', '5', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:47:50', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 6, '广播权', '6', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:48:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 7, '其他权利', '7', 'product_right', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:48:11', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 1, '草稿', '1', 'apply_status', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:48:52', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 4, '已发布', '4', 'apply_status', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:49:04', 'admin', '2022-01-10 23:01:08', NULL);
INSERT INTO `sys_dict_data` VALUES (112, 3, '不通过', '3', 'apply_status', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:49:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 1, '待支付', '1', 'order_status', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:49:53', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 2, '已完成', '2', 'order_status', NULL, 'default', 'N', '0', 'admin', '2022-01-10 15:50:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 2, '待审核', '2', 'apply_status', NULL, 'default', 'N', '0', 'admin', '2022-01-10 23:01:24', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (116, 1, '通过', '1', 'check_status', NULL, 'default', 'N', '0', 'admin', '2022-01-11 15:34:42', 'admin', '2022-01-11 15:37:22', NULL);
INSERT INTO `sys_dict_data` VALUES (117, 2, '不通过', '2', 'check_status', NULL, 'default', 'N', '0', 'admin', '2022-01-11 15:35:12', 'admin', '2022-01-11 15:37:26', NULL);
INSERT INTO `sys_dict_data` VALUES (118, 3, '取消订单', '3', 'order_status', NULL, 'default', 'N', '0', 'admin', '2022-01-11 16:31:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 5, '下架', '5', 'apply_status', NULL, 'default', 'N', '0', 'gly', '2022-01-11 22:19:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (121, 1, '未发送', '1', 'message_status', NULL, 'primary', 'N', '0', 'admin', '2022-01-12 22:01:27', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (122, 2, '已发送', '2', 'message_status', NULL, 'success', 'N', '0', 'admin', '2022-01-12 22:01:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (123, 1, '未读', '1', 'redonly_status', NULL, 'primary', 'N', '0', 'admin', '2022-01-12 22:02:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (124, 2, '已读', '2', 'redonly_status', NULL, 'success', 'N', '0', 'admin', '2022-01-12 22:02:31', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-01-04 14:51:54', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '产品类型', 'product_type', '0', 'admin', '2022-01-10 15:43:31', '', NULL, '产品类型列表');
INSERT INTO `sys_dict_type` VALUES (101, '版权权利', 'product_right', '0', 'admin', '2022-01-10 15:45:58', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (102, '申请状态', 'apply_status', '0', 'admin', '2022-01-10 15:48:36', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (103, '订单状态', 'order_status', '0', 'admin', '2022-01-10 15:49:38', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (104, '审核状态', 'check_status', '0', 'admin', '2022-01-11 15:34:27', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (105, '消息状态', 'message_status', '0', 'admin', '2022-01-12 22:00:56', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (106, '阅读状态', 'redonly_status', '0', 'admin', '2022-01-12 22:02:07', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-01-04 14:51:54', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-01-04 14:51:54', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-01-04 14:51:54', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '提示信息',
  `access_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 283 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 14:03:59');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 14:11:23');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 14:11:28');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 14:13:49');
INSERT INTO `sys_logininfor` VALUES (104, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-06 14:13:56');
INSERT INTO `sys_logininfor` VALUES (105, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-06 14:14:50');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 14:14:54');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 14:16:35');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 14:16:40');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 14:42:10');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 21:16:58');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 21:17:08');
INSERT INTO `sys_logininfor` VALUES (112, 'zs', '127.0.0.1', '0', '注册成功', '2022-01-06 21:17:22');
INSERT INTO `sys_logininfor` VALUES (113, 'zs', '127.0.0.1', '0', '登录成功', '2022-01-06 21:17:37');
INSERT INTO `sys_logininfor` VALUES (114, 'zs', '127.0.0.1', '0', '退出成功', '2022-01-06 21:17:51');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 21:17:57');
INSERT INTO `sys_logininfor` VALUES (116, 'zs', '127.0.0.1', '0', '注册成功', '2022-01-06 21:46:20');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 21:46:28');
INSERT INTO `sys_logininfor` VALUES (118, 'zs', '127.0.0.1', '0', '登录成功', '2022-01-06 21:46:32');
INSERT INTO `sys_logininfor` VALUES (119, 'zs', '127.0.0.1', '0', '退出成功', '2022-01-06 21:46:54');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 21:47:01');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 21:47:47');
INSERT INTO `sys_logininfor` VALUES (122, 'zs', '127.0.0.1', '0', '登录成功', '2022-01-06 21:47:53');
INSERT INTO `sys_logininfor` VALUES (123, 'zs', '127.0.0.1', '0', '退出成功', '2022-01-06 21:52:12');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 21:52:17');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-06 21:53:31');
INSERT INTO `sys_logininfor` VALUES (126, 'zs', '127.0.0.1', '0', '登录成功', '2022-01-06 21:53:36');
INSERT INTO `sys_logininfor` VALUES (127, 'zs', '127.0.0.1', '0', '退出成功', '2022-01-06 21:55:34');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-06 21:55:39');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 15:14:38');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 15:15:42');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 15:18:51');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 15:54:48');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 15:54:52');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:00:57');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:01:01');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:01:25');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:01:29');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:04:17');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:04:20');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:07:57');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:08:00');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:08:21');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:08:24');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:10:55');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:10:59');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 16:14:25');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 16:14:28');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 21:00:46');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-10 22:33:29');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-10 22:33:34');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 11:04:47');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 11:20:58');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 11:21:03');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 11:34:36');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 11:34:45');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 11:36:36');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 11:36:42');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 14:57:11');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 15:03:05');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 15:03:09');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 15:04:16');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 15:04:21');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 15:13:27');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 15:13:30');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 15:57:17');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 15:57:29');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 16:26:31');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 16:26:48');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 16:29:24');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 16:29:27');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 16:30:44');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 16:30:48');
INSERT INTO `sys_logininfor` VALUES (173, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 16:43:06');
INSERT INTO `sys_logininfor` VALUES (174, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 16:43:11');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 16:55:15');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 16:55:20');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 17:38:40');
INSERT INTO `sys_logininfor` VALUES (178, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 17:38:43');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 17:40:25');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 17:40:28');
INSERT INTO `sys_logininfor` VALUES (181, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 17:45:27');
INSERT INTO `sys_logininfor` VALUES (182, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 17:45:30');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 21:09:06');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 21:15:39');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 21:15:43');
INSERT INTO `sys_logininfor` VALUES (186, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 21:17:01');
INSERT INTO `sys_logininfor` VALUES (187, 'zs', '127.0.0.1', '1', '对不起，您的账号已被删除', '2022-01-11 21:17:10');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 21:17:19');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 21:17:49');
INSERT INTO `sys_logininfor` VALUES (190, 'zzx', '127.0.0.1', '1', '用户密码错误', '2022-01-11 21:17:57');
INSERT INTO `sys_logininfor` VALUES (191, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 21:18:04');
INSERT INTO `sys_logininfor` VALUES (192, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 21:18:16');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 21:18:42');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 21:26:47');
INSERT INTO `sys_logininfor` VALUES (195, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 21:26:56');
INSERT INTO `sys_logininfor` VALUES (196, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 21:31:04');
INSERT INTO `sys_logininfor` VALUES (197, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 21:31:09');
INSERT INTO `sys_logininfor` VALUES (198, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 21:47:52');
INSERT INTO `sys_logininfor` VALUES (199, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 21:48:01');
INSERT INTO `sys_logininfor` VALUES (200, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 21:53:37');
INSERT INTO `sys_logininfor` VALUES (201, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 21:53:41');
INSERT INTO `sys_logininfor` VALUES (202, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 21:57:32');
INSERT INTO `sys_logininfor` VALUES (203, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 21:57:42');
INSERT INTO `sys_logininfor` VALUES (204, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 21:59:57');
INSERT INTO `sys_logininfor` VALUES (205, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 22:00:03');
INSERT INTO `sys_logininfor` VALUES (206, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 22:00:50');
INSERT INTO `sys_logininfor` VALUES (207, 'gly', '127.0.0.1', '1', '用户密码错误', '2022-01-11 22:00:59');
INSERT INTO `sys_logininfor` VALUES (208, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:01:05');
INSERT INTO `sys_logininfor` VALUES (209, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:01:27');
INSERT INTO `sys_logininfor` VALUES (210, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 22:01:33');
INSERT INTO `sys_logininfor` VALUES (211, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 22:02:16');
INSERT INTO `sys_logininfor` VALUES (212, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:02:23');
INSERT INTO `sys_logininfor` VALUES (213, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:03:59');
INSERT INTO `sys_logininfor` VALUES (214, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 22:04:05');
INSERT INTO `sys_logininfor` VALUES (215, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 22:04:17');
INSERT INTO `sys_logininfor` VALUES (216, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 22:04:21');
INSERT INTO `sys_logininfor` VALUES (217, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 22:06:04');
INSERT INTO `sys_logininfor` VALUES (218, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:06:14');
INSERT INTO `sys_logininfor` VALUES (219, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:07:50');
INSERT INTO `sys_logininfor` VALUES (220, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 22:07:55');
INSERT INTO `sys_logininfor` VALUES (221, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 22:08:51');
INSERT INTO `sys_logininfor` VALUES (222, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:09:02');
INSERT INTO `sys_logininfor` VALUES (223, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:19:42');
INSERT INTO `sys_logininfor` VALUES (224, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:19:48');
INSERT INTO `sys_logininfor` VALUES (225, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:20:27');
INSERT INTO `sys_logininfor` VALUES (226, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 22:20:33');
INSERT INTO `sys_logininfor` VALUES (227, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 22:21:16');
INSERT INTO `sys_logininfor` VALUES (228, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:21:21');
INSERT INTO `sys_logininfor` VALUES (229, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:21:37');
INSERT INTO `sys_logininfor` VALUES (230, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-11 22:21:43');
INSERT INTO `sys_logininfor` VALUES (231, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-11 22:35:42');
INSERT INTO `sys_logininfor` VALUES (232, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 22:35:46');
INSERT INTO `sys_logininfor` VALUES (233, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 22:49:23');
INSERT INTO `sys_logininfor` VALUES (234, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:49:28');
INSERT INTO `sys_logininfor` VALUES (235, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:53:14');
INSERT INTO `sys_logininfor` VALUES (236, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 22:53:20');
INSERT INTO `sys_logininfor` VALUES (237, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 22:55:03');
INSERT INTO `sys_logininfor` VALUES (238, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:55:07');
INSERT INTO `sys_logininfor` VALUES (239, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:55:46');
INSERT INTO `sys_logininfor` VALUES (240, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-11 22:55:51');
INSERT INTO `sys_logininfor` VALUES (241, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-11 22:56:53');
INSERT INTO `sys_logininfor` VALUES (242, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-11 22:56:58');
INSERT INTO `sys_logininfor` VALUES (243, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-11 22:58:14');
INSERT INTO `sys_logininfor` VALUES (244, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 10:09:27');
INSERT INTO `sys_logininfor` VALUES (245, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-12 10:11:06');
INSERT INTO `sys_logininfor` VALUES (246, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-12 10:11:11');
INSERT INTO `sys_logininfor` VALUES (247, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-12 10:12:38');
INSERT INTO `sys_logininfor` VALUES (248, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 10:12:42');
INSERT INTO `sys_logininfor` VALUES (249, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-12 10:36:56');
INSERT INTO `sys_logininfor` VALUES (250, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 10:37:00');
INSERT INTO `sys_logininfor` VALUES (251, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 15:06:56');
INSERT INTO `sys_logininfor` VALUES (252, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 21:30:46');
INSERT INTO `sys_logininfor` VALUES (253, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-12 21:34:01');
INSERT INTO `sys_logininfor` VALUES (254, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 21:34:05');
INSERT INTO `sys_logininfor` VALUES (255, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-12 22:02:38');
INSERT INTO `sys_logininfor` VALUES (256, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-12 22:02:47');
INSERT INTO `sys_logininfor` VALUES (257, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-13 21:16:59');
INSERT INTO `sys_logininfor` VALUES (258, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-13 21:34:27');
INSERT INTO `sys_logininfor` VALUES (259, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-13 21:34:33');
INSERT INTO `sys_logininfor` VALUES (260, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-13 22:36:35');
INSERT INTO `sys_logininfor` VALUES (261, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-13 22:36:41');
INSERT INTO `sys_logininfor` VALUES (262, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-13 22:37:58');
INSERT INTO `sys_logininfor` VALUES (263, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-13 22:38:05');
INSERT INTO `sys_logininfor` VALUES (264, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-13 22:43:47');
INSERT INTO `sys_logininfor` VALUES (265, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-13 22:43:53');
INSERT INTO `sys_logininfor` VALUES (266, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-13 22:46:34');
INSERT INTO `sys_logininfor` VALUES (267, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-13 22:46:39');
INSERT INTO `sys_logininfor` VALUES (268, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-13 22:46:44');
INSERT INTO `sys_logininfor` VALUES (269, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-13 22:46:49');
INSERT INTO `sys_logininfor` VALUES (270, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-13 22:47:08');
INSERT INTO `sys_logininfor` VALUES (271, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-13 22:47:17');
INSERT INTO `sys_logininfor` VALUES (272, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-13 22:49:45');
INSERT INTO `sys_logininfor` VALUES (273, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-13 22:49:51');
INSERT INTO `sys_logininfor` VALUES (274, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-13 22:50:22');
INSERT INTO `sys_logininfor` VALUES (275, 'zzx', '127.0.0.1', '0', '登录成功', '2022-01-13 22:50:31');
INSERT INTO `sys_logininfor` VALUES (276, 'zzx', '127.0.0.1', '0', '退出成功', '2022-01-13 22:51:25');
INSERT INTO `sys_logininfor` VALUES (277, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-13 22:51:31');
INSERT INTO `sys_logininfor` VALUES (278, 'gly', '127.0.0.1', '0', '退出成功', '2022-01-13 22:54:07');
INSERT INTO `sys_logininfor` VALUES (279, 'admin', '127.0.0.1', '0', '登录成功', '2022-01-13 22:54:12');
INSERT INTO `sys_logininfor` VALUES (280, 'admin', '127.0.0.1', '0', '退出成功', '2022-01-13 22:54:35');
INSERT INTO `sys_logininfor` VALUES (281, 'gly', '127.0.0.1', '0', '登录成功', '2022-01-13 22:54:42');
INSERT INTO `sys_logininfor` VALUES (282, 'admin', '127.0.0.1', '0', '登录成功', '2022-02-18 16:53:19');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '组件路径',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2063 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '用户权限管理', 0, 66, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-12 21:33:56', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 88, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-11 15:55:55', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 99, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-11 15:56:01', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-01-04 14:51:53', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-01-04 14:51:53', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-01-04 14:51:53', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '1', 'system:dept:list', 'tree', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-06 14:16:26', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '1', 'system:post:list', 'post', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-06 14:16:31', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-01-04 14:51:53', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-01-04 14:51:53', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '动态发布', 2043, 9, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-12 21:33:20', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 10, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-01-04 14:51:53', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-01-04 14:51:53', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-01-04 14:51:53', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', '', 0, 0, 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 'admin', '2022-01-04 14:51:53', '', NULL, '流量控制菜单');
INSERT INTO `sys_menu` VALUES (112, 'Nacos控制台', 2, 4, 'http://localhost:8848/nacos', '', 0, 0, 'C', '0', '0', 'monitor:nacos:list', 'nacos', 'admin', '2022-01-04 14:51:53', '', NULL, '服务治理菜单');
INSERT INTO `sys_menu` VALUES (113, 'Admin控制台', 2, 5, 'http://localhost:9100/login', '', 0, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-01-04 14:51:53', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-01-04 14:51:53', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-01-04 14:51:53', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'http://localhost:8080/swagger-ui/index.html', '', 0, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-01-04 14:51:53', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', 1, 0, 'C', '0', '0', 'system:operlog:list', 'form', 'admin', '2022-01-04 14:51:53', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', 1, 0, 'C', '0', '0', 'system:logininfor:list', 'logininfor', 'admin', '2022-01-04 14:51:53', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'system:operlog:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'system:operlog:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'system:operlog:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'system:logininfor:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'system:logininfor:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'system:logininfor:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1062, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1063, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1064, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1065, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2001, '产品管理', 2007, 1, 'index', 'system/product/index', 1, 0, 'C', '0', '0', 'system:product:list', 'system', 'admin', '2022-01-10 15:58:36', 'admin', '2022-01-11 15:10:40', '版权管理菜单');
INSERT INTO `sys_menu` VALUES (2002, '产品管理查询', 2001, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-01-10 15:58:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '产品管理新增', 2001, 2, '#', '', 1, 0, 'F', '0', '0', 'system:product:add', '#', 'admin', '2022-01-10 15:58:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '产品管理修改', 2001, 3, '#', '', 1, 0, 'F', '0', '0', 'system:product:edit', '#', 'admin', '2022-01-10 15:58:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '产品管理删除', 2001, 4, '#', '', 1, 0, 'F', '0', '0', 'system:product:remove', '#', 'admin', '2022-01-10 15:58:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '产品管理导出', 2001, 5, '#', '', 1, 0, 'F', '0', '0', 'system:product:export', '#', 'admin', '2022-01-10 15:58:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '版权管理', 0, 1, 'product', NULL, 1, 0, 'M', '0', '0', NULL, 'lock', 'admin', '2022-01-10 16:03:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '已发布产品', 2007, 2, 'Myproduct', 'system/product/Myproduct', 1, 0, 'C', '0', '0', 'system:product:list', 'example', 'admin', '2022-01-11 15:00:21', 'admin', '2022-01-11 15:10:52', '');
INSERT INTO `sys_menu` VALUES (2009, '查询', 2008, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-01-11 15:12:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '导出', 2008, 5, '#', '', 1, 0, 'F', '0', '0', 'system:product:export', '#', 'admin', '2022-01-11 15:12:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '产品审核', 2007, 3, 'check', 'system/product/check', 1, 0, 'C', '0', '0', 'system:product:list', 'client', 'admin', '2022-01-11 15:17:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '查询', 2011, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-01-11 15:20:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, '修改', 2011, 3, '#', '', 1, 0, 'F', '0', '0', 'system:product:edit', '#', 'admin', '2022-01-11 15:20:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '订单管理', 0, 3, 'orderManage', NULL, 1, 0, 'M', '0', '0', NULL, 'zip', 'admin', '2022-01-11 15:54:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '产品列表', 2020, 1, 'productList', 'system/product/prodectList', 1, 0, 'C', '0', '0', 'system:product:list', 'chart', 'admin', '2022-01-11 15:55:17', 'admin', '2022-01-11 15:55:30', '');
INSERT INTO `sys_menu` VALUES (2022, '查询', 2021, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-01-11 15:57:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '购物车', 2020, 1, 'order', 'system/product/order', 1, 0, 'C', '0', '0', 'system:order:list', 'education', 'admin', '2022-01-11 16:26:23', 'admin', '2022-01-11 16:30:40', '订单菜单');
INSERT INTO `sys_menu` VALUES (2024, '订单查询', 2023, 1, '#', '', 1, 0, 'F', '0', '0', 'system:order:query', '#', 'admin', '2022-01-11 16:26:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '订单新增', 2023, 2, '#', '', 1, 0, 'F', '0', '0', 'system:order:add', '#', 'admin', '2022-01-11 16:26:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '订单修改', 2023, 3, '#', '', 1, 0, 'F', '0', '0', 'system:order:edit', '#', 'admin', '2022-01-11 16:26:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '订单删除', 2023, 4, '#', '', 1, 0, 'F', '0', '0', 'system:order:remove', '#', 'admin', '2022-01-11 16:26:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '订单导出', 2023, 5, '#', '', 1, 0, 'F', '0', '0', 'system:order:export', '#', 'admin', '2022-01-11 16:26:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '我的订单', 2020, 3, 'historyOrder', 'system/product/historyOrder', 1, 0, 'C', '0', '0', 'system:order:list', 'guide', 'admin', '2022-01-11 17:37:34', 'admin', '2022-01-11 17:40:20', '');
INSERT INTO `sys_menu` VALUES (2030, '产品发布', 2007, 4, 'push', 'system/product/push', 1, 0, 'C', '0', '0', 'system:product:list', 'client', 'admin', '2022-01-11 21:20:02', 'admin', '2022-01-11 21:20:17', '');
INSERT INTO `sys_menu` VALUES (2031, '查询', 2030, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-01-11 21:21:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '修改', 2030, 3, '#', '', 1, 0, 'F', '0', '0', 'system:product:edit', '#', 'admin', '2022-01-11 21:21:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '所有订单', 2020, 4, 'allOrder', 'system/product/allOrder', 1, 0, 'C', '0', '0', 'system:order:list', 'documentation', 'admin', '2022-01-11 21:44:57', 'admin', '2022-01-11 21:45:18', '');
INSERT INTO `sys_menu` VALUES (2034, '查询', 2033, 1, '#', '', 1, 0, 'F', '0', '0', 'system:order:list', '#', 'admin', '2022-01-11 21:46:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '查询', 2029, 1, '#', '', 1, 0, 'F', '0', '0', 'system:order:list', '#', 'admin', '2022-01-11 21:46:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '删除', 2030, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:remove', '#', 'admin', '2022-01-11 21:59:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '详情', 2033, 1, '#', '', 1, 0, 'F', '0', '0', 'system:order:query', '#', 'admin', '2022-01-11 22:07:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '详情', 2030, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-01-11 22:11:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '新增', 2030, 1, '#', '', 1, 0, 'F', '0', '0', 'system:product:add', '#', 'admin', '2022-01-11 22:55:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '最近动态', 0, 4, 'news', NULL, 1, 0, 'M', '0', '0', '', 'bug', 'admin', '2022-01-12 11:08:05', 'admin', '2022-01-12 11:08:10', '');
INSERT INTO `sys_menu` VALUES (2041, '新闻动态', 2040, 1, 'newsIndex', 'system/product/newsIndex', 1, 0, 'C', '0', '0', 'system:product:list', 'fullscreen', 'admin', '2022-01-12 11:10:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '公告动态', 2040, 2, 'noticeIndex', 'system/product/noticeIndex', 1, 0, 'C', '0', '0', 'system:product:list', 'client', 'admin', '2022-01-12 11:12:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '动态管理', 0, 4, 'noticeMange', NULL, 1, 0, 'M', '0', '0', NULL, 'client', 'admin', '2022-01-12 21:33:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '通知管理', 0, 5, 'messageMange', NULL, 1, 0, 'M', '0', '0', NULL, 'education', 'admin', '2022-01-12 21:35:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '消息发布', 2044, 1, 'message', 'system/product/messageIndex', 1, 0, 'C', '0', '0', 'system:message:list', 'email', 'admin', '2022-01-12 21:58:18', 'admin', '2022-01-13 21:31:54', '消息发布菜单');
INSERT INTO `sys_menu` VALUES (2046, '消息发布查询', 2045, 1, '#', '', 1, 0, 'F', '0', '0', 'system:message:query', '#', 'admin', '2022-01-12 21:58:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '消息发布新增', 2045, 2, '#', '', 1, 0, 'F', '0', '0', 'system:message:add', '#', 'admin', '2022-01-12 21:58:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '消息发布修改', 2045, 3, '#', '', 1, 0, 'F', '0', '0', 'system:message:edit', '#', 'admin', '2022-01-12 21:58:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '消息发布删除', 2045, 4, '#', '', 1, 0, 'F', '0', '0', 'system:message:remove', '#', 'admin', '2022-01-12 21:58:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '消息发布导出', 2045, 5, '#', '', 1, 0, 'F', '0', '0', 'system:message:export', '#', 'admin', '2022-01-12 21:58:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '我的消息', 2044, 2, 'myMessage', 'system/product/myMessage', 1, 0, 'C', '0', '0', 'system:message:list', 'message', 'admin', '2022-01-13 21:31:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '消息用户查询', 2051, 1, '#', '', 1, 0, 'F', '0', '0', 'system:message:query', '#', 'admin', '2022-01-13 21:34:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '消息用户新增', 2051, 2, '#', '', 1, 0, 'F', '0', '0', 'system:message:add', '#', 'admin', '2022-01-13 21:34:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '消息用户修改', 2051, 3, '#', '', 1, 0, 'F', '0', '0', 'system:message:edit', '#', 'admin', '2022-01-13 21:34:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '消息用户删除', 2051, 4, '#', '', 1, 0, 'F', '0', '0', 'system:message:remove', '#', 'admin', '2022-01-13 21:34:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '动态查询', 2041, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-01-13 22:43:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '动态新增', 2041, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-01-13 22:43:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2058, '动态修改', 2041, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-01-13 22:43:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '动态删除', 2041, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-01-13 22:43:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '动态导出', 2041, 5, '#', '', 1, 0, 'F', '0', '0', 'system:notice:export', '#', 'admin', '2022-01-13 22:43:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '动态列表', 2041, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:list', '#', 'admin', '2022-01-13 22:46:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '消息修改', 2051, 1, '#', '', 1, 0, 'F', '0', '0', 'system:messageuser:edit', '#', 'admin', '2022-01-13 22:49:41', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '新闻：国务院发布版权系统', '1', 0x3C703EE78988E69D83E7B3BBE7BB9FE698AFE794B14A647A7A78E585ACE58FB8E88194E59088E59BBDE5AEB6E4B880E8B5B7E5BC80E58F91E5AE8CE68890EFBC8CE4BB96E5B086E4BC9AE59CA8E69CAAE69DA5E78988E69D83E79A84E9A286E59F9FE4B88AE5819AE587BAE69C80E5A4A7E79A84E8B4A1E78CAE3C2F703E3C703E3C696D67207372633D22687474703A2F2F3132372E302E302E313A393030302F66696C65732F70726F647563742F63336265323236386264323134323237623535383766653263363930336265622E6A7067223E3C2F703E, '0', 'admin', '2022-01-04 14:51:54', 'admin', '2022-01-12 11:00:09', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 版权系统凌晨维护', '2', 0x3C703EE7BBB4E68AA4E58685E5AEB93132313231323C2F703E, '0', 'admin', '2022-01-04 14:51:54', 'gly', '2022-01-13 22:52:04', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 323 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '菜单管理', 3, 'com.jdzzx.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/4', '127.0.0.1', '', NULL, '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2022-01-06 14:10:52');
INSERT INTO `sys_oper_log` VALUES (101, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050,2,109,1051,1052,1053,110,1054,1055,1056,1057,1058,1059,111,112,113,3,114,115,1060,1061,1063,1062,1064,1065,116],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:11:11');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 3, 'com.jdzzx.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/4', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:11:18');
INSERT INTO `sys_oper_log` VALUES (103, '用户管理', 1, 'com.jdzzx.system.controller.SysUserController.add()', 'POST', 1, 'admin', NULL, '/user', '127.0.0.1', '', '{\"phonenumber\":\"13107085063\",\"admin\":false,\"password\":\"$2a$10$EuyxMK7iIqi5C2ZddR/1d.rZqYACEIAVSBXHlkXgDVekVw/CQPOJ2\",\"postIds\":[],\"nickName\":\"郑zzx\",\"sex\":\"0\",\"params\":{},\"userName\":\"zzx\",\"userId\":100,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:13:23');
INSERT INTO `sys_oper_log` VALUES (104, '用户管理', 3, 'com.jdzzx.system.controller.SysUserController.remove()', 'DELETE', 1, 'zzx', NULL, '/user/2', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:14:09');
INSERT INTO `sys_oper_log` VALUES (105, '用户管理', 1, 'com.jdzzx.system.controller.SysUserController.add()', 'POST', 1, 'admin', NULL, '/user', '127.0.0.1', '', '{\"admin\":false,\"password\":\"$2a$10$L2cnQjS/B6iEbNjmsh0JG.R41IjKzwUHJtDHPEPmjp7z/CQIvjgCy\",\"postIds\":[],\"nickName\":\"管理员\",\"params\":{},\"userName\":\"gly\",\"userId\":101,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:15:34');
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"1\",\"icon\":\"tree\",\"orderNum\":\"4\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:16:11');
INSERT INTO `sys_oper_log` VALUES (107, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"tree\",\"orderNum\":\"4\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:16:26');
INSERT INTO `sys_oper_log` VALUES (108, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"post\",\"orderNum\":\"5\",\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:16:31');
INSERT INTO `sys_oper_log` VALUES (109, '角色管理', 1, 'com.jdzzx.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"deptIds\":[],\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:18:16');
INSERT INTO `sys_oper_log` VALUES (110, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:18:39');
INSERT INTO `sys_oper_log` VALUES (111, '用户管理', 4, 'com.jdzzx.system.controller.SysUserController.insertAuthRole()', 'PUT', 1, 'admin', NULL, '/user/authRole', '127.0.0.1', '', '101 [100]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:18:53');
INSERT INTO `sys_oper_log` VALUES (112, '用户管理', 4, 'com.jdzzx.system.controller.SysUserController.insertAuthRole()', 'PUT', 1, 'admin', NULL, '/user/authRole', '127.0.0.1', '', '100 [2]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 14:19:05');
INSERT INTO `sys_oper_log` VALUES (113, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 21:47:39');
INSERT INTO `sys_oper_log` VALUES (114, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/role/dataScope', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-06 21:53:25');
INSERT INTO `sys_oper_log` VALUES (115, '用户管理', 3, 'com.jdzzx.system.controller.SysUserController.remove()', 'DELETE', 1, 'admin', NULL, '/user/103', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:15:33');
INSERT INTO `sys_oper_log` VALUES (116, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_product', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:41:23');
INSERT INTO `sys_oper_log` VALUES (117, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"产品类型\",\"remark\":\"产品类型列表\",\"params\":{},\"dictType\":\"product_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:43:31');
INSERT INTO `sys_oper_log` VALUES (118, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"product_type\",\"dictLabel\":\"作品\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:45:02');
INSERT INTO `sys_oper_log` VALUES (119, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"product_type\",\"dictLabel\":\"图片\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:45:15');
INSERT INTO `sys_oper_log` VALUES (120, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"product_type\",\"dictLabel\":\"软件\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:45:25');
INSERT INTO `sys_oper_log` VALUES (121, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"版权权利\",\"params\":{},\"dictType\":\"product_right\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:45:58');
INSERT INTO `sys_oper_log` VALUES (122, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"复制权\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:46:43');
INSERT INTO `sys_oper_log` VALUES (123, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"发行权\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:46:55');
INSERT INTO `sys_oper_log` VALUES (124, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"出租权\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:47:07');
INSERT INTO `sys_oper_log` VALUES (125, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"出租权\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641800827000,\"dictCode\":105,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:47:14');
INSERT INTO `sys_oper_log` VALUES (126, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"4\",\"listClass\":\"default\",\"dictSort\":4,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"展览权\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:47:31');
INSERT INTO `sys_oper_log` VALUES (127, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"5\",\"listClass\":\"default\",\"dictSort\":5,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"表演权\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:47:50');
INSERT INTO `sys_oper_log` VALUES (128, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"6\",\"listClass\":\"default\",\"dictSort\":6,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"广播权\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:48:01');
INSERT INTO `sys_oper_log` VALUES (129, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"7\",\"listClass\":\"default\",\"dictSort\":7,\"params\":{},\"dictType\":\"product_right\",\"dictLabel\":\"其他权利\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:48:11');
INSERT INTO `sys_oper_log` VALUES (130, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"申请状态\",\"params\":{},\"dictType\":\"apply_status\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:48:36');
INSERT INTO `sys_oper_log` VALUES (131, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"草稿\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:48:52');
INSERT INTO `sys_oper_log` VALUES (132, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"通过\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"通过\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:49:04');
INSERT INTO `sys_oper_log` VALUES (133, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"不通过\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:49:15');
INSERT INTO `sys_oper_log` VALUES (134, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"订单状态\",\"params\":{},\"dictType\":\"order_status\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:49:38');
INSERT INTO `sys_oper_log` VALUES (135, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"order_status\",\"dictLabel\":\"待支付\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:49:53');
INSERT INTO `sys_oper_log` VALUES (136, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"order_status\",\"dictLabel\":\"已完成\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:50:16');
INSERT INTO `sys_oper_log` VALUES (137, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"lock\",\"orderNum\":\"1\",\"menuName\":\"版权管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:54:13');
INSERT INTO `sys_oper_log` VALUES (138, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_product', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 15:55:56');
INSERT INTO `sys_oper_log` VALUES (139, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"lock\",\"orderNum\":\"1\",\"menuName\":\"版权管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"product\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 16:03:10');
INSERT INTO `sys_oper_log` VALUES (140, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product', '127.0.0.1', '', '{\"productMoney\":\"100\",\"params\":{},\"productName\":\"测试\",\"productRight\":\"1\",\"createTime\":1641802705882,\"id\":1,\"authMethod\":\"普通授权\",\"productType\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 16:18:26');
INSERT INTO `sys_oper_log` VALUES (141, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/1', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:01:06');
INSERT INTO `sys_oper_log` VALUES (142, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product', '127.0.0.1', '', '{\"productMoney\":\"100\",\"params\":{},\"productName\":\"安徒生童话\",\"productRight\":\"1\",\"createTime\":1641819795163,\"id\":2,\"authMethod\":\"普通授权\",\"productNo\":\"CP20220110210315\",\"productType\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:03:15');
INSERT INTO `sys_oper_log` VALUES (143, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/2', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:29:14');
INSERT INTO `sys_oper_log` VALUES (144, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product', '127.0.0.1', '', '{\"productMoney\":\"12\",\"params\":{},\"productName\":\"测试\",\"productRight\":\"1,2\",\"createTime\":1641821450427,\"id\":3,\"authMethod\":\"12\",\"productNo\":\"CP20220110213050\",\"productType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:30:50');
INSERT INTO `sys_oper_log` VALUES (145, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'PUT', 1, 'admin', NULL, '/product', '127.0.0.1', '', '{\"productMoney\":\"12\",\"params\":{},\"productName\":\"测试\",\"productRight\":\"1,2,3\",\"createTime\":1641821450000,\"id\":3,\"authMethod\":\"12\",\"productNo\":\"CP20220110213050\",\"productType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:36:30');
INSERT INTO `sys_oper_log` VALUES (146, '代码生成', 3, 'com.jdzzx.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/1', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:47:31');
INSERT INTO `sys_oper_log` VALUES (147, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_product_apply', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:47:39');
INSERT INTO `sys_oper_log` VALUES (148, '代码生成', 3, 'com.jdzzx.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/2', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:48:53');
INSERT INTO `sys_oper_log` VALUES (149, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_product', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:48:58');
INSERT INTO `sys_oper_log` VALUES (150, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_product', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:49:49');
INSERT INTO `sys_oper_log` VALUES (151, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product/add', '127.0.0.1', '', '{\"productMoney\":\"100\",\"params\":{},\"userId\":1,\"productName\":\"测试\",\"productRight\":\"1,2,3\",\"createTime\":1641822934711,\"id\":4,\"authMethod\":\"普通授权\",\"applyStatus\":\"1\",\"productNo\":\"CP20220110215534\",\"productType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:55:34');
INSERT INTO `sys_oper_log` VALUES (152, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"已发布\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641800944000,\"dictCode\":111,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 21:57:07');
INSERT INTO `sys_oper_log` VALUES (153, '用户头像', 2, 'com.jdzzx.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '127.0.0.1', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://127.0.0.1:9000/files/product/4bb1264262bc433b8669984c7e9f1a47.jpeg\",\"code\":200}', 0, NULL, '2022-01-10 22:01:46');
INSERT INTO `sys_oper_log` VALUES (154, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product/add', '127.0.0.1', '', '{\"params\":{},\"userId\":1,\"productRight\":\"\",\"createTime\":1641824019970,\"id\":5,\"applyStatus\":\"1\",\"productNo\":\"CP20220110221339\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:13:39');
INSERT INTO `sys_oper_log` VALUES (155, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/5', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:13:42');
INSERT INTO `sys_oper_log` VALUES (156, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/4', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:25:05');
INSERT INTO `sys_oper_log` VALUES (157, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product/add', '127.0.0.1', '', '{\"productMoney\":\"100\",\"params\":{},\"userId\":1,\"productName\":\"测试\",\"productRight\":\"1,2\",\"createTime\":1641824736159,\"id\":6,\"authMethod\":\"普通授权\",\"applyStatus\":\"1\",\"productNo\":\"CP20220110222536\",\"productType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:25:36');
INSERT INTO `sys_oper_log` VALUES (158, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/6', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:25:58');
INSERT INTO `sys_oper_log` VALUES (159, '版权管理', 1, 'com.jdzzx.system.controller.BusinessProductController.add()', 'POST', 1, 'admin', NULL, '/product/add', '127.0.0.1', '', '{\"productMoney\":\"100\",\"params\":{},\"userId\":1,\"productName\":\"测试\",\"productRight\":\"1,2\",\"createTime\":1641824778905,\"id\":7,\"authMethod\":\"测试\",\"applyStatus\":\"1\",\"productNo\":\"CP20220110222618\",\"productType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:26:18');
INSERT INTO `sys_oper_log` VALUES (160, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/7', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:30:03');
INSERT INTO `sys_oper_log` VALUES (161, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/8', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 22:51:05');
INSERT INTO `sys_oper_log` VALUES (162, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"4\",\"listClass\":\"default\",\"dictSort\":4,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"已发布\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641800944000,\"dictCode\":111,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 23:01:08');
INSERT INTO `sys_oper_log` VALUES (163, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"待审核\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-10 23:01:24');
INSERT INTO `sys_oper_log` VALUES (164, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/11', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:24:14');
INSERT INTO `sys_oper_log` VALUES (165, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/12', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:25:49');
INSERT INTO `sys_oper_log` VALUES (166, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/13', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:29:06');
INSERT INTO `sys_oper_log` VALUES (167, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/14', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:41:29');
INSERT INTO `sys_oper_log` VALUES (168, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/15', '127.0.0.1', '', NULL, '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2022-01-11 11:42:20');
INSERT INTO `sys_oper_log` VALUES (169, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/15', '127.0.0.1', '', NULL, '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2022-01-11 11:42:24');
INSERT INTO `sys_oper_log` VALUES (170, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/15', '127.0.0.1', '', NULL, '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2022-01-11 11:42:37');
INSERT INTO `sys_oper_log` VALUES (171, '版权管理', 3, 'com.jdzzx.system.controller.BusinessProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/16', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:46:17');
INSERT INTO `sys_oper_log` VALUES (172, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:46:20');
INSERT INTO `sys_oper_log` VALUES (173, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:47:41');
INSERT INTO `sys_oper_log` VALUES (174, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:47:58');
INSERT INTO `sys_oper_log` VALUES (175, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:48:18');
INSERT INTO `sys_oper_log` VALUES (176, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:49:02');
INSERT INTO `sys_oper_log` VALUES (177, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 11:49:42');
INSERT INTO `sys_oper_log` VALUES (178, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"example\",\"orderNum\":\"2\",\"menuName\":\"我的产品\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"/system/product/Myproduct\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:00:21');
INSERT INTO `sys_oper_log` VALUES (179, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"example\",\"orderNum\":\"2\",\"menuName\":\"我的产品\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"product\",\"component\":\"system/product/Myproduct\",\"children\":[],\"createTime\":1641884421000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2008,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:01:26');
INSERT INTO `sys_oper_log` VALUES (180, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"产品管理\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"index\",\"component\":\"system/product/index\",\"children\":[],\"createTime\":1641801516000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:04:02');
INSERT INTO `sys_oper_log` VALUES (181, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"example\",\"orderNum\":\"2\",\"menuName\":\"我的产品\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"Myproduct\",\"component\":\"system/product/Myproduct\",\"children\":[],\"createTime\":1641884421000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2008,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:04:10');
INSERT INTO `sys_oper_log` VALUES (182, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"system\",\"orderNum\":\"1\",\"menuName\":\"产品管理\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"index\",\"component\":\"system/product/index\",\"children\":[],\"createTime\":1641801516000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:10:40');
INSERT INTO `sys_oper_log` VALUES (183, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"example\",\"orderNum\":\"2\",\"menuName\":\"历史产品\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"Myproduct\",\"component\":\"system/product/Myproduct\",\"children\":[],\"createTime\":1641884421000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2008,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:10:52');
INSERT INTO `sys_oper_log` VALUES (184, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"client\",\"orderNum\":\"3\",\"menuName\":\"产品审核\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"check\",\"component\":\"system/product/check\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:17:50');
INSERT INTO `sys_oper_log` VALUES (185, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"审核状态\",\"params\":{},\"dictType\":\"check_status\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:34:27');
INSERT INTO `sys_oper_log` VALUES (186, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"check_status\",\"dictLabel\":\"通过\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:34:42');
INSERT INTO `sys_oper_log` VALUES (187, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"check_status\",\"dictLabel\":\"不通过\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:35:12');
INSERT INTO `sys_oper_log` VALUES (188, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"check_status\",\"dictLabel\":\"不通过\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641886512000,\"dictCode\":117,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:35:19');
INSERT INTO `sys_oper_log` VALUES (189, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"4\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"check_status\",\"dictLabel\":\"通过\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641886482000,\"dictCode\":116,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:35:48');
INSERT INTO `sys_oper_log` VALUES (190, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"check_status\",\"dictLabel\":\"通过\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641886482000,\"dictCode\":116,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:37:22');
INSERT INTO `sys_oper_log` VALUES (191, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"check_status\",\"dictLabel\":\"不通过\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641886512000,\"dictCode\":117,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:37:26');
INSERT INTO `sys_oper_log` VALUES (192, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":10,\"applyStatus\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:52:10');
INSERT INTO `sys_oper_log` VALUES (193, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"zip\",\"orderNum\":\"3\",\"menuName\":\"订单管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"order\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:54:06');
INSERT INTO `sys_oper_log` VALUES (194, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":\"1\",\"menuName\":\"产品列表\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"productList\",\"component\":\"system/product/prodectList\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:55:17');
INSERT INTO `sys_oper_log` VALUES (195, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":\"1\",\"menuName\":\"产品列表\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"productList\",\"component\":\"system/product/prodectList\",\"children\":[],\"createTime\":1641887717000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2021,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:55:30');
INSERT INTO `sys_oper_log` VALUES (196, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"system\",\"orderNum\":\"4\",\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:55:40');
INSERT INTO `sys_oper_log` VALUES (197, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"monitor\",\"orderNum\":\"88\",\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:55:55');
INSERT INTO `sys_oper_log` VALUES (198, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"tool\",\"orderNum\":\"99\",\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:56:01');
INSERT INTO `sys_oper_log` VALUES (199, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"system\",\"orderNum\":\"66\",\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 15:56:06');
INSERT INTO `sys_oper_log` VALUES (200, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_order', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 16:13:53');
INSERT INTO `sys_oper_log` VALUES (201, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_order', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 16:18:59');
INSERT INTO `sys_oper_log` VALUES (202, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"1\",\"menuName\":\"订单\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"order\",\"component\":\"system/product/order\",\"children\":[],\"createTime\":1641889583000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2023,\"menuType\":\"C\",\"perms\":\"system:order:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 16:30:40');
INSERT INTO `sys_oper_log` VALUES (203, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"order_status\",\"dictLabel\":\"取消订单\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 16:31:51');
INSERT INTO `sys_oper_log` VALUES (204, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'admin', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":10,\"params\":{},\"userId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 16:56:45');
INSERT INTO `sys_oper_log` VALUES (205, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'admin', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":10,\"params\":{},\"userId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 17:04:00');
INSERT INTO `sys_oper_log` VALUES (206, '订单', 3, 'com.jdzzx.system.controller.BusinessOrderController.remove()', 'DELETE', 1, 'admin', NULL, '/order/2', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 17:05:24');
INSERT INTO `sys_oper_log` VALUES (207, '订单', 2, 'com.jdzzx.system.controller.BusinessOrderController.edit()', 'POST', 1, 'admin', NULL, '/order/edit', '127.0.0.1', '', '{\"orderStatus\":\"2\",\"params\":{},\"id\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 17:35:13');
INSERT INTO `sys_oper_log` VALUES (208, '订单', 2, 'com.jdzzx.system.controller.BusinessOrderController.edit()', 'POST', 1, 'admin', NULL, '/order/edit', '127.0.0.1', '', '{\"orderStatus\":\"2\",\"params\":{},\"id\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 17:35:45');
INSERT INTO `sys_oper_log` VALUES (209, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"guide\",\"orderNum\":\"3\",\"menuName\":\"历史订单\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"historyOrder\",\"component\":\"system/order/historyOrder\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:order:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 17:37:34');
INSERT INTO `sys_oper_log` VALUES (210, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"guide\",\"orderNum\":\"3\",\"menuName\":\"历史订单\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"historyOrder\",\"component\":\"system/product/historyOrder\",\"children\":[],\"createTime\":1641893854000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2029,\"menuType\":\"C\",\"perms\":\"system:order:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 17:40:20');
INSERT INTO `sys_oper_log` VALUES (211, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":9,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:09:32');
INSERT INTO `sys_oper_log` VALUES (212, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":9,\"applyStatus\":\"3\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:09:44');
INSERT INTO `sys_oper_log` VALUES (213, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":9,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:10:14');
INSERT INTO `sys_oper_log` VALUES (214, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'admin', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":9,\"applyStatus\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:10:20');
INSERT INTO `sys_oper_log` VALUES (215, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'admin', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":9,\"params\":{},\"userId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:10:40');
INSERT INTO `sys_oper_log` VALUES (216, '订单', 3, 'com.jdzzx.system.controller.BusinessOrderController.remove()', 'DELETE', 1, 'admin', NULL, '/order/3', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:14:02');
INSERT INTO `sys_oper_log` VALUES (217, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'admin', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":9,\"params\":{},\"userId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:14:43');
INSERT INTO `sys_oper_log` VALUES (218, '订单', 2, 'com.jdzzx.system.controller.BusinessOrderController.edit()', 'POST', 1, 'admin', NULL, '/order/edit', '127.0.0.1', '', '{\"orderStatus\":\"3\",\"params\":{},\"id\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:14:47');
INSERT INTO `sys_oper_log` VALUES (219, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"client\",\"orderNum\":\"4\",\"menuName\":\"产品发布\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"push\",\"component\":\"system/product/push\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:20:02');
INSERT INTO `sys_oper_log` VALUES (220, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"client\",\"orderNum\":\"4\",\"menuName\":\"产品发布\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"push\",\"component\":\"system/product/push\",\"children\":[],\"createTime\":1641907202000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2030,\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:20:17');
INSERT INTO `sys_oper_log` VALUES (221, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2007,1,2001,2002,2003,2004,2005,2006,2008,2009,2010,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,100,1001,1002,1003,1004,1005,1006,1007],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:42:36');
INSERT INTO `sys_oper_log` VALUES (222, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,1,2011,2012,2013,2030,2031,2032,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:43:19');
INSERT INTO `sys_oper_log` VALUES (223, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"documentation\",\"orderNum\":\"4\",\"menuName\":\"所有订单\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"allOrder\",\"component\":\"system/product/allOrder\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:product:allOrder\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:44:57');
INSERT INTO `sys_oper_log` VALUES (224, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"documentation\",\"orderNum\":\"4\",\"menuName\":\"所有订单\",\"params\":{},\"parentId\":2020,\"isCache\":\"0\",\"path\":\"allOrder\",\"component\":\"system/product/allOrder\",\"children\":[],\"createTime\":1641908697000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2033,\"menuType\":\"C\",\"perms\":\"system:order:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:45:18');
INSERT INTO `sys_oper_log` VALUES (225, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2032,2021,2022,2033,2034,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:47:48');
INSERT INTO `sys_oper_log` VALUES (226, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'zzx', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":18,\"applyStatus\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 21:57:27');
INSERT INTO `sys_oper_log` VALUES (227, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2032,2021,2022,2033,2034,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:00:20');
INSERT INTO `sys_oper_log` VALUES (228, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/role/dataScope', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"deptIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:00:33');
INSERT INTO `sys_oper_log` VALUES (229, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":18,\"applyStatus\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:01:19');
INSERT INTO `sys_oper_log` VALUES (230, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'zzx', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":9,\"params\":{},\"userId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:01:47');
INSERT INTO `sys_oper_log` VALUES (231, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'zzx', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":10,\"params\":{},\"userId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:01:52');
INSERT INTO `sys_oper_log` VALUES (232, '订单', 2, 'com.jdzzx.system.controller.BusinessOrderController.edit()', 'POST', 1, 'zzx', NULL, '/order/edit', '127.0.0.1', '', '{\"orderStatus\":\"2\",\"params\":{},\"id\":5}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:02:06');
INSERT INTO `sys_oper_log` VALUES (233, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'gly', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"gly\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2020,2007,2011,2012,2013,2030,2031,2036,2032,2033,2034,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:03:42');
INSERT INTO `sys_oper_log` VALUES (234, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2032,2033,2034,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,107,1041,1042,1043,1044],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:05:56');
INSERT INTO `sys_oper_log` VALUES (235, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2032,2033,2034,2037,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,107,1041,1042,1043,1044],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:08:48');
INSERT INTO `sys_oper_log` VALUES (236, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":9,\"applyStatus\":\"5\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:17:59');
INSERT INTO `sys_oper_log` VALUES (237, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'gly', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"5\",\"listClass\":\"default\",\"dictSort\":5,\"params\":{},\"dictType\":\"apply_status\",\"dictLabel\":\"下架\",\"createBy\":\"gly\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:19:38');
INSERT INTO `sys_oper_log` VALUES (238, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":9,\"applyStatus\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:20:07');
INSERT INTO `sys_oper_log` VALUES (239, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":18,\"applyStatus\":\"5\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:20:13');
INSERT INTO `sys_oper_log` VALUES (240, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":18,\"applyStatus\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:20:16');
INSERT INTO `sys_oper_log` VALUES (241, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":18,\"applyStatus\":\"5\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:20:23');
INSERT INTO `sys_oper_log` VALUES (242, '版权管理', 2, 'com.jdzzx.system.controller.BusinessProductController.edit()', 'POST', 1, 'gly', NULL, '/product/edit', '127.0.0.1', '', '{\"params\":{},\"id\":18,\"applyStatus\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:21:33');
INSERT INTO `sys_oper_log` VALUES (243, '订单', 2, 'com.jdzzx.system.controller.BusinessOrderController.edit()', 'POST', 1, 'zzx', NULL, '/order/edit', '127.0.0.1', '', '{\"orderStatus\":\"2\",\"params\":{},\"id\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:22:22');
INSERT INTO `sys_oper_log` VALUES (244, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2038,2032,2033,2034,2037,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,107,1041,1042,1043,1044],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:54:59');
INSERT INTO `sys_oper_log` VALUES (245, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2038,2039,2032,2033,2034,2037,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,105,1026,1027,1028,1029,1030,107,1041,1042,1043,1044],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-11 22:56:12');
INSERT INTO `sys_oper_log` VALUES (246, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>维护内容</p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"admin\",\"noticeType\":\"1\",\"remark\":\"管理员\",\"params\":{},\"noticeId\":2,\"noticeTitle\":\"维护通知：2018-07-01 版权系统凌晨维护\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 10:13:14');
INSERT INTO `sys_oper_log` VALUES (247, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>新版本内容</p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"admin\",\"noticeType\":\"1\",\"remark\":\"管理员\",\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：2018-07-01 版权系统新版本发布啦\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 10:13:34');
INSERT INTO `sys_oper_log` VALUES (248, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>维护内容</p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"admin\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"updateTime\":1641953594000,\"params\":{},\"noticeId\":2,\"noticeTitle\":\"维护通知：2018-07-01 版权系统凌晨维护\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 10:13:50');
INSERT INTO `sys_oper_log` VALUES (249, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>新版本内容<img src=\\\"http://127.0.0.1:9000/files/product/c3be2268bd214227b5587fe2c6903beb.jpg\\\"></p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"admin\",\"noticeType\":\"1\",\"remark\":\"管理员\",\"updateTime\":1641953614000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：2018-07-01 版权系统新版本发布啦\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 10:15:56');
INSERT INTO `sys_oper_log` VALUES (250, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"warning\",\"dictSort\":1,\"remark\":\"通知\",\"params\":{},\"dictType\":\"sys_notice_type\",\"dictLabel\":\"新闻\",\"createBy\":\"admin\",\"default\":true,\"isDefault\":\"Y\",\"cssClass\":\"\",\"createTime\":1641279114000,\"dictCode\":14,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 10:36:45');
INSERT INTO `sys_oper_log` VALUES (251, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>新版本内容<img src=\\\"http://127.0.0.1:9000/files/product/c3be2268bd214227b5587fe2c6903beb.jpg\\\"></p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"admin\",\"noticeType\":\"1\",\"remark\":\"管理员\",\"updateTime\":1641953756000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"新闻：国务院发布版权系统\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 10:58:24');
INSERT INTO `sys_oper_log` VALUES (252, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>版权系统是由Jdzzx公司联合国家一起开发完成，他将会在未来版权的领域上做出最大的贡献</p><p><img src=\\\"http://127.0.0.1:9000/files/product/c3be2268bd214227b5587fe2c6903beb.jpg\\\"></p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"admin\",\"noticeType\":\"1\",\"remark\":\"管理员\",\"updateTime\":1641956304000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"新闻：国务院发布版权系统\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:00:09');
INSERT INTO `sys_oper_log` VALUES (253, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"sys_notice_type\",\"dictLabel\":\"动态\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:03:34');
INSERT INTO `sys_oper_log` VALUES (254, '字典数据', 2, 'com.jdzzx.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"3\",\"listClass\":\"danger\",\"dictSort\":3,\"params\":{},\"dictType\":\"sys_notice_type\",\"dictLabel\":\"动态\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1641956614000,\"dictCode\":120,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:05:02');
INSERT INTO `sys_oper_log` VALUES (255, '字典类型', 3, 'com.jdzzx.system.controller.SysDictDataController.remove()', 'DELETE', 1, 'admin', NULL, '/dict/data/120', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:06:30');
INSERT INTO `sys_oper_log` VALUES (256, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"bug\",\"orderNum\":\"3\",\"menuName\":\"最近动态\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"news\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:08:05');
INSERT INTO `sys_oper_log` VALUES (257, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"bug\",\"orderNum\":\"4\",\"menuName\":\"最近动态\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"news\",\"children\":[],\"createTime\":1641956885000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2040,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:08:10');
INSERT INTO `sys_oper_log` VALUES (258, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"fullscreen\",\"orderNum\":\"1\",\"menuName\":\"新闻动态\",\"params\":{},\"parentId\":2040,\"isCache\":\"0\",\"path\":\"newsIndex\",\"component\":\"system/product/newsIndex\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:10:47');
INSERT INTO `sys_oper_log` VALUES (259, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"client\",\"orderNum\":\"2\",\"menuName\":\"公告动态\",\"params\":{},\"parentId\":2040,\"isCache\":\"0\",\"path\":\"noticeIndex\",\"component\":\"system/product/noticeIndex\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:product:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 11:12:06');
INSERT INTO `sys_oper_log` VALUES (260, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'admin', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":9,\"params\":{},\"userId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 15:25:44');
INSERT INTO `sys_oper_log` VALUES (261, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":\"9\",\"menuName\":\"动态发布\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:32:00');
INSERT INTO `sys_oper_log` VALUES (262, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"client\",\"orderNum\":\"4\",\"menuName\":\"动态管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"noticeMange\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:33:07');
INSERT INTO `sys_oper_log` VALUES (263, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":\"9\",\"menuName\":\"动态发布\",\"params\":{},\"parentId\":2043,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:33:20');
INSERT INTO `sys_oper_log` VALUES (264, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"system\",\"orderNum\":\"66\",\"menuName\":\"用户权限管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1641279113000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:33:56');
INSERT INTO `sys_oper_log` VALUES (265, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"5\",\"menuName\":\"通知管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"messageMange\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:35:29');
INSERT INTO `sys_oper_log` VALUES (266, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2007,2020,1,2001,2002,2003,2004,2005,2006,2008,2009,2010,2021,2022,2023,2024,2025,2026,2027,2028,2029,2040,2041,2042,100,1001,1002,1003,1004,1005,1006,1007],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:37:23');
INSERT INTO `sys_oper_log` VALUES (267, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_notice_message', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:37:42');
INSERT INTO `sys_oper_log` VALUES (268, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:40:27');
INSERT INTO `sys_oper_log` VALUES (269, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:47:01');
INSERT INTO `sys_oper_log` VALUES (270, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:47:09');
INSERT INTO `sys_oper_log` VALUES (271, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_message_user', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:48:08');
INSERT INTO `sys_oper_log` VALUES (272, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:49:39');
INSERT INTO `sys_oper_log` VALUES (273, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:53:01');
INSERT INTO `sys_oper_log` VALUES (274, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:53:02');
INSERT INTO `sys_oper_log` VALUES (275, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:55:35');
INSERT INTO `sys_oper_log` VALUES (276, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:55:37');
INSERT INTO `sys_oper_log` VALUES (277, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:01');
INSERT INTO `sys_oper_log` VALUES (278, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:38');
INSERT INTO `sys_oper_log` VALUES (279, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:40');
INSERT INTO `sys_oper_log` VALUES (280, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:52');
INSERT INTO `sys_oper_log` VALUES (281, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:53');
INSERT INTO `sys_oper_log` VALUES (282, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_notice_message', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:54');
INSERT INTO `sys_oper_log` VALUES (283, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 21:56:55');
INSERT INTO `sys_oper_log` VALUES (284, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"消息状态\",\"params\":{},\"dictType\":\"message_status\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:00:57');
INSERT INTO `sys_oper_log` VALUES (285, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"primary\",\"dictSort\":1,\"params\":{},\"dictType\":\"message_status\",\"dictLabel\":\"未发送\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:01:27');
INSERT INTO `sys_oper_log` VALUES (286, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"success\",\"dictSort\":2,\"params\":{},\"dictType\":\"message_status\",\"dictLabel\":\"已发送\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:01:39');
INSERT INTO `sys_oper_log` VALUES (287, '字典类型', 1, 'com.jdzzx.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '127.0.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"阅读状态\",\"params\":{},\"dictType\":\"redonly_status\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:02:07');
INSERT INTO `sys_oper_log` VALUES (288, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"1\",\"listClass\":\"primary\",\"dictSort\":1,\"params\":{},\"dictType\":\"redonly_status\",\"dictLabel\":\"未读\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:02:23');
INSERT INTO `sys_oper_log` VALUES (289, '字典数据', 1, 'com.jdzzx.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '127.0.0.1', '', '{\"dictValue\":\"2\",\"listClass\":\"success\",\"dictSort\":2,\"params\":{},\"dictType\":\"redonly_status\",\"dictLabel\":\"已读\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:02:31');
INSERT INTO `sys_oper_log` VALUES (290, '消息发布', 1, 'com.jdzzx.system.controller.BusinessNoticeMessageController.add()', 'POST', 1, 'admin', NULL, '/message/add', '127.0.0.1', '', '{\"messageStatus\":\"1\",\"createTime\":1641996537496,\"messageId\":10,\"messageTitle\":\"系统消息：您的版权申请已经通过了\",\"remark\":\"厉害了\",\"params\":{},\"messageContent\":\"您的版权申请已经通过了\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:08:57');
INSERT INTO `sys_oper_log` VALUES (291, '消息发布', 1, 'com.jdzzx.system.controller.BusinessNoticeMessageController.sendMessage()', 'POST', 1, 'admin', NULL, '/message/sendMessage', '127.0.0.1', '', '{\"messageStatus\":\"1\",\"createTime\":1641996537000,\"userIds\":[100,101],\"messageId\":10,\"messageTitle\":\"系统消息：您的版权申请已经通过了\",\"remark\":\"厉害了\",\"params\":{},\"messageContent\":\"您的版权申请已经通过了\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:36:23');
INSERT INTO `sys_oper_log` VALUES (292, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:40:54');
INSERT INTO `sys_oper_log` VALUES (293, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.genCode()', 'GET', 1, 'admin', NULL, '/gen/genCode/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:41:09');
INSERT INTO `sys_oper_log` VALUES (294, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:42:11');
INSERT INTO `sys_oper_log` VALUES (295, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:42:41');
INSERT INTO `sys_oper_log` VALUES (296, '代码生成', 2, 'com.jdzzx.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/gen/synchDb/business_message_user', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:43:09');
INSERT INTO `sys_oper_log` VALUES (297, '代码生成', 3, 'com.jdzzx.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/6', '127.0.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:43:21');
INSERT INTO `sys_oper_log` VALUES (298, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'business_message_user', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:43:30');
INSERT INTO `sys_oper_log` VALUES (299, '代码生成', 8, 'com.jdzzx.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2022-01-12 22:43:34');
INSERT INTO `sys_oper_log` VALUES (300, '消息发布', 1, 'com.jdzzx.system.controller.BusinessNoticeMessageController.sendMessage()', 'POST', 1, 'admin', NULL, '/message/sendMessage', '127.0.0.1', '', '{\"messageStatus\":\"1\",\"createTime\":1641996537000,\"userIds\":[100,101],\"messageId\":10,\"messageTitle\":\"系统消息：您的版权申请已经通过了\",\"remark\":\"厉害了\",\"params\":{},\"messageContent\":\"您的版权申请已经通过了\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-12 22:46:38');
INSERT INTO `sys_oper_log` VALUES (301, '菜单管理', 1, 'com.jdzzx.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":\"2\",\"menuName\":\"我的消息\",\"params\":{},\"parentId\":2044,\"isCache\":\"0\",\"path\":\"myMessage\",\"component\":\"system/product/myMessage\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:message:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 21:31:34');
INSERT INTO `sys_oper_log` VALUES (302, '菜单管理', 2, 'com.jdzzx.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"email\",\"orderNum\":\"1\",\"menuName\":\"消息发布\",\"params\":{},\"parentId\":2044,\"isCache\":\"0\",\"path\":\"message\",\"component\":\"system/product/messageIndex\",\"children\":[],\"createTime\":1641995898000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2045,\"menuType\":\"C\",\"perms\":\"system:message:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 21:31:54');
INSERT INTO `sys_oper_log` VALUES (303, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'admin', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"updateTime\":1642083547054,\"params\":{},\"id\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:19:07');
INSERT INTO `sys_oper_log` VALUES (304, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'admin', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"updateTime\":1642083551252,\"params\":{},\"id\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:19:11');
INSERT INTO `sys_oper_log` VALUES (305, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'admin', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"updateTime\":1642083625378,\"params\":{},\"id\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:20:25');
INSERT INTO `sys_oper_log` VALUES (306, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2007,2020,2044,2001,2002,2003,2004,2005,2006,2008,2009,2010,2021,2022,2023,2024,2025,2026,2027,2028,2029,2040,2041,2042,2051,2052,2053,2054,2055],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:35:48');
INSERT INTO `sys_oper_log` VALUES (307, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2038,2039,2032,2033,2034,2037,2043,107,1041,1042,1043,1044,2044,2045,2046,2047,2048,2049,2050,2051,2052,2053,2054,2055,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:36:32');
INSERT INTO `sys_oper_log` VALUES (308, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2007,2020,2044,2001,2002,2003,2004,2005,2006,2008,2009,2010,2021,2022,2023,2024,2025,2026,2027,2028,2029,2040,2041,2056,2057,2058,2059,2060,2042,2051,2052,2053,2054,2055],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:43:42');
INSERT INTO `sys_oper_log` VALUES (309, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2007,2020,2044,2001,2002,2003,2004,2005,2006,2008,2009,2010,2021,2022,2023,2024,2025,2026,2027,2028,2029,2040,2041,2056,2061,2057,2058,2059,2060,2042,2051,2052,2053,2054,2055],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:47:05');
INSERT INTO `sys_oper_log` VALUES (310, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'zzx', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"params\":{},\"id\":10}', 'null', 1, '', '2022-01-13 22:47:42');
INSERT INTO `sys_oper_log` VALUES (311, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'zzx', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"params\":{},\"id\":10}', 'null', 1, '', '2022-01-13 22:47:52');
INSERT INTO `sys_oper_log` VALUES (312, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"5\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641279113000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2007,2020,2044,2001,2002,2003,2004,2005,2006,2008,2009,2010,2021,2022,2023,2024,2025,2026,2027,2028,2029,2040,2041,2056,2061,2057,2058,2059,2060,2042,2051,2052,2062,2053,2054,2055],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:50:17');
INSERT INTO `sys_oper_log` VALUES (313, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'zzx', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"updateTime\":1642085440933,\"params\":{},\"id\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:50:40');
INSERT INTO `sys_oper_log` VALUES (314, '订单', 1, 'com.jdzzx.system.controller.BusinessOrderController.add()', 'POST', 1, 'zzx', NULL, '/order/add', '127.0.0.1', '', '{\"productId\":9,\"params\":{},\"userId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:51:09');
INSERT INTO `sys_oper_log` VALUES (315, '订单', 2, 'com.jdzzx.system.controller.BusinessOrderController.edit()', 'POST', 1, 'zzx', NULL, '/order/edit', '127.0.0.1', '', '{\"orderStatus\":\"2\",\"params\":{},\"id\":8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:51:16');
INSERT INTO `sys_oper_log` VALUES (316, '通知公告', 2, 'com.jdzzx.system.controller.SysNoticeController.edit()', 'PUT', 1, 'gly', NULL, '/notice', '127.0.0.1', '', '{\"noticeContent\":\"<p>维护内容121212</p>\",\"createBy\":\"admin\",\"createTime\":1641279114000,\"updateBy\":\"gly\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"updateTime\":1641953630000,\"params\":{},\"noticeId\":2,\"noticeTitle\":\"维护通知：2018-07-01 版权系统凌晨维护\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:52:04');
INSERT INTO `sys_oper_log` VALUES (317, '消息发布', 2, 'com.jdzzx.system.controller.BusinessNoticeMessageController.edit()', 'POST', 1, 'gly', NULL, '/message/edit', '127.0.0.1', '', '{\"messageStatus\":\"2\",\"createTime\":1641996537000,\"messageId\":10,\"messageTitle\":\"系统消息：您的版权申请已经通过了\",\"remark\":\"厉害了\",\"params\":{},\"messageContent\":\"您的版权申请已经通过了121221\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:52:15');
INSERT INTO `sys_oper_log` VALUES (318, '消息发布', 1, 'com.jdzzx.system.controller.BusinessNoticeMessageController.sendMessage()', 'POST', 1, 'gly', NULL, '/message/sendMessage', '127.0.0.1', '', '{\"messageStatus\":\"2\",\"createTime\":1641996537000,\"userIds\":[100],\"messageId\":10,\"messageTitle\":\"系统消息：您的版权申请已经通过了\",\"remark\":\"厉害了\",\"params\":{},\"messageContent\":\"您的版权申请已经通过了121221\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:52:21');
INSERT INTO `sys_oper_log` VALUES (319, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'gly', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"params\":{},\"id\":11}', 'null', 1, '', '2022-01-13 22:52:27');
INSERT INTO `sys_oper_log` VALUES (320, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'gly', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"gly\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2011,2012,2013,2030,2031,2036,2038,2039,2032,2020,2033,2034,2037,2043,107,1041,1042,1043,1044,2044,2045,2046,2047,2048,2049,2050,2051,2052,2053,2054,2055,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:53:04');
INSERT INTO `sys_oper_log` VALUES (321, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'gly', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"params\":{},\"id\":11}', 'null', 1, '', '2022-01-13 22:53:09');
INSERT INTO `sys_oper_log` VALUES (322, '角色管理', 2, 'com.jdzzx.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1641449896000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"administrator\",\"roleName\":\"管理员\",\"menuIds\":[2007,2020,1,2011,2012,2013,2030,2031,2036,2038,2039,2032,2033,2034,2037,2043,107,1041,1042,1043,1044,2044,2045,2046,2047,2048,2049,2050,2051,2052,2062,2053,2054,2055,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:54:27');
INSERT INTO `sys_oper_log` VALUES (323, '消息接收者', 2, 'com.jdzzx.system.controller.BusinessMessageUserController.edit()', 'POST', 1, 'gly', NULL, '/messageuser/edit', '127.0.0.1', '', '{\"redonlyStatus\":\"2\",\"updateTime\":1642085688553,\"params\":{},\"id\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-01-13 22:54:48');
INSERT INTO `sys_oper_log` VALUES (324, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'bus_account', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-02-18 16:56:03');
INSERT INTO `sys_oper_log` VALUES (325, '代码生成', 6, 'com.jdzzx.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'bus_account', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-02-18 17:09:55');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-01-04 14:51:53', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-01-04 14:51:53', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-01-04 14:51:53', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '5', 1, 1, '0', '0', 'admin', '2022-01-04 14:51:53', 'admin', '2022-01-13 22:50:17', '普通角色');
INSERT INTO `sys_role` VALUES (100, '管理员', 'administrator', 2, '1', 1, 1, '0', '0', 'admin', '2022-01-06 14:18:16', 'admin', '2022-01-13 22:54:27', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2008);
INSERT INTO `sys_role_menu` VALUES (2, 2009);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2020);
INSERT INTO `sys_role_menu` VALUES (2, 2021);
INSERT INTO `sys_role_menu` VALUES (2, 2022);
INSERT INTO `sys_role_menu` VALUES (2, 2023);
INSERT INTO `sys_role_menu` VALUES (2, 2024);
INSERT INTO `sys_role_menu` VALUES (2, 2025);
INSERT INTO `sys_role_menu` VALUES (2, 2026);
INSERT INTO `sys_role_menu` VALUES (2, 2027);
INSERT INTO `sys_role_menu` VALUES (2, 2028);
INSERT INTO `sys_role_menu` VALUES (2, 2029);
INSERT INTO `sys_role_menu` VALUES (2, 2040);
INSERT INTO `sys_role_menu` VALUES (2, 2041);
INSERT INTO `sys_role_menu` VALUES (2, 2042);
INSERT INTO `sys_role_menu` VALUES (2, 2044);
INSERT INTO `sys_role_menu` VALUES (2, 2051);
INSERT INTO `sys_role_menu` VALUES (2, 2052);
INSERT INTO `sys_role_menu` VALUES (2, 2053);
INSERT INTO `sys_role_menu` VALUES (2, 2054);
INSERT INTO `sys_role_menu` VALUES (2, 2055);
INSERT INTO `sys_role_menu` VALUES (2, 2056);
INSERT INTO `sys_role_menu` VALUES (2, 2057);
INSERT INTO `sys_role_menu` VALUES (2, 2058);
INSERT INTO `sys_role_menu` VALUES (2, 2059);
INSERT INTO `sys_role_menu` VALUES (2, 2060);
INSERT INTO `sys_role_menu` VALUES (2, 2061);
INSERT INTO `sys_role_menu` VALUES (2, 2062);
INSERT INTO `sys_role_menu` VALUES (100, 1);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 101);
INSERT INTO `sys_role_menu` VALUES (100, 102);
INSERT INTO `sys_role_menu` VALUES (100, 107);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 1007);
INSERT INTO `sys_role_menu` VALUES (100, 1008);
INSERT INTO `sys_role_menu` VALUES (100, 1009);
INSERT INTO `sys_role_menu` VALUES (100, 1010);
INSERT INTO `sys_role_menu` VALUES (100, 1011);
INSERT INTO `sys_role_menu` VALUES (100, 1012);
INSERT INTO `sys_role_menu` VALUES (100, 1013);
INSERT INTO `sys_role_menu` VALUES (100, 1014);
INSERT INTO `sys_role_menu` VALUES (100, 1015);
INSERT INTO `sys_role_menu` VALUES (100, 1016);
INSERT INTO `sys_role_menu` VALUES (100, 1041);
INSERT INTO `sys_role_menu` VALUES (100, 1042);
INSERT INTO `sys_role_menu` VALUES (100, 1043);
INSERT INTO `sys_role_menu` VALUES (100, 1044);
INSERT INTO `sys_role_menu` VALUES (100, 2007);
INSERT INTO `sys_role_menu` VALUES (100, 2011);
INSERT INTO `sys_role_menu` VALUES (100, 2012);
INSERT INTO `sys_role_menu` VALUES (100, 2013);
INSERT INTO `sys_role_menu` VALUES (100, 2020);
INSERT INTO `sys_role_menu` VALUES (100, 2030);
INSERT INTO `sys_role_menu` VALUES (100, 2031);
INSERT INTO `sys_role_menu` VALUES (100, 2032);
INSERT INTO `sys_role_menu` VALUES (100, 2033);
INSERT INTO `sys_role_menu` VALUES (100, 2034);
INSERT INTO `sys_role_menu` VALUES (100, 2036);
INSERT INTO `sys_role_menu` VALUES (100, 2037);
INSERT INTO `sys_role_menu` VALUES (100, 2038);
INSERT INTO `sys_role_menu` VALUES (100, 2039);
INSERT INTO `sys_role_menu` VALUES (100, 2043);
INSERT INTO `sys_role_menu` VALUES (100, 2044);
INSERT INTO `sys_role_menu` VALUES (100, 2045);
INSERT INTO `sys_role_menu` VALUES (100, 2046);
INSERT INTO `sys_role_menu` VALUES (100, 2047);
INSERT INTO `sys_role_menu` VALUES (100, 2048);
INSERT INTO `sys_role_menu` VALUES (100, 2049);
INSERT INTO `sys_role_menu` VALUES (100, 2050);
INSERT INTO `sys_role_menu` VALUES (100, 2051);
INSERT INTO `sys_role_menu` VALUES (100, 2052);
INSERT INTO `sys_role_menu` VALUES (100, 2053);
INSERT INTO `sys_role_menu` VALUES (100, 2054);
INSERT INTO `sys_role_menu` VALUES (100, 2055);
INSERT INTO `sys_role_menu` VALUES (100, 2062);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', 'jdzzx', '00', 'jdzzx@163.com', '15888888888', '1', 'http://127.0.0.1:9000/files/product/4bb1264262bc433b8669984c7e9f1a47.jpeg', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-01-04 14:51:53', 'admin', '2022-01-04 14:51:53', '', NULL, '管理员');
INSERT INTO `sys_user` VALUES (100, NULL, 'zzx', '郑zzx', '00', '', '13107085063', '0', '', '$2a$10$EuyxMK7iIqi5C2ZddR/1d.rZqYACEIAVSBXHlkXgDVekVw/CQPOJ2', '0', '0', '', NULL, 'admin', '2022-01-06 14:13:23', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (101, NULL, 'gly', '管理员', '00', '', '', '0', '', '$2a$10$L2cnQjS/B6iEbNjmsh0JG.R41IjKzwUHJtDHPEPmjp7z/CQIvjgCy', '0', '0', '', NULL, 'admin', '2022-01-06 14:15:34', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (100, 2);
INSERT INTO `sys_user_role` VALUES (101, 100);

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `branch_id` bigint NOT NULL COMMENT 'branch transaction id',
  `xid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'global transaction id',
  `context` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'undo_log context,such as serialization',
  `rollback_info` longblob NOT NULL COMMENT 'rollback info',
  `log_status` int NOT NULL COMMENT '0:normal status,1:defense status',
  `log_created` datetime(6) NOT NULL COMMENT 'create datetime',
  `log_modified` datetime(6) NOT NULL COMMENT 'modify datetime',
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'AT transaction mode undo table' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of undo_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
