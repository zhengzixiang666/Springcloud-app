import request from '@/utils/request'

// 查询版权管理列表
export function listProduct(query) {
  return request({
    url: '/system/product/list',
    method: 'get',
    params: query
  })
}

// 查询版权管理详细
export function getProduct(id) {
  return request({
    url: '/system/product/' + id,
    method: 'get'
  })
}

// 新增版权管理
export function addProduct(data) {
  return request({
    url: '/system/product/add',
    method: 'post',
    data: data
  })
}

// 修改版权管理
export function updateProduct(data) {
  return request({
    url: '/system/product/edit',
    method: 'post',
    data: data
  })
}

// 删除版权管理
export function delProduct(id) {
  return request({
    url: '/system/product/' + id,
    method: 'delete'
  })
}