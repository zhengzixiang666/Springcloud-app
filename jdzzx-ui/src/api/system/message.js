import request from '@/utils/request'

// 查询消息发布列表
export function listMessage(query) {
  return request({
    url: '/system/message/list',
    method: 'get',
    params: query
  })
}

// 查询消息发布详细
export function getMessage(messageId) {
  return request({
    url: '/system/message/' + messageId,
    method: 'get'
  })
}

// 新增消息发布
export function addMessage(data) {
  return request({
    url: '/system/message/add',
    method: 'post',
    data: data
  })
}

// 发送消息
export function sendMessage(data) {
    return request({
      url: '/system/message/sendMessage',
      method: 'post',
      data: data
    })
}


// 发送消息
export function selectByUserId(query) {
  return request({
    url: '/system/messageuser/selectByUserId',
    method: 'get',
    params: query
  })
}


// 修改消息发布
export function updateMessage(data) {
  return request({
    url: '/system/message/edit',
    method: 'post',
    data: data
  })
}

// 修改消息发布
export function updateMessageUser(data) {
  return request({
    url: '/system/messageuser/edit',
    method: 'post',
    data: data
  })
}


// 删除消息发布
export function delMessage(messageId) {
  return request({
    url: '/system/message/' + messageId,
    method: 'delete'
  })
}



