package com.jdzzx.modules.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 监控中心
 * 
 * @author zzx
 */
@EnableAdminServer
@SpringBootApplication
public class JdzzxMonitorApplication
{
    public static void main(String[] args)
    {
    	ConfigurableApplicationContext context =SpringApplication.run(JdzzxMonitorApplication.class, args);
    	Environment environment = context.getBean(Environment.class);
    	System.out.println("(♥◠‿◠)ﾉﾞ  监控中心启动成功,端口号为"+environment.getProperty("local.server.port")+"   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
