package com.jdzzx.order.mapper;

import java.util.List;
import com.jdzzx.order.domain.BusAccount;

/**
 * 订单Mapper接口
 * 
 * @author zzx
 * @date 2022-02-18
 */
public interface BusAccountMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public BusAccount selectBusAccountById(Long id);

    /**
     * 查询订单列表
     * 
     * @param busAccount 订单
     * @return 订单集合
     */
    public List<BusAccount> selectBusAccountList(BusAccount busAccount);

    /**
     * 新增订单
     * 
     * @param busAccount 订单
     * @return 结果
     */
    public int insertBusAccount(BusAccount busAccount);

    /**
     * 修改订单
     * 
     * @param busAccount 订单
     * @return 结果
     */
    public int updateBusAccount(BusAccount busAccount);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteBusAccountById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusAccountByIds(Long[] ids);
}