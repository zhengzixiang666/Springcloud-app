package com.jdzzx.order.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jdzzx.common.log.annotation.Log;
import com.jdzzx.common.log.enums.BusinessType;
import com.jdzzx.common.security.annotation.PreAuthorize;
import com.jdzzx.order.domain.BusAccount;
import com.jdzzx.order.service.IBusAccountService;
import com.jdzzx.common.core.web.controller.BaseController;
import com.jdzzx.common.core.web.domain.AjaxResult;
import com.jdzzx.common.core.utils.poi.ExcelUtil;
import com.jdzzx.common.core.web.page.TableDataInfo;

/**
 * 订单Controller
 * 
 * @author zzx
 * @date 2022-02-18
 */
@RestController
@RequestMapping("/account")
public class BusAccountController extends BaseController
{
    @Autowired
    private IBusAccountService busAccountService;

    /**
     * 查询订单列表
     */
    @PreAuthorize(hasPermi = "order:account:list")
    @GetMapping("/list")
    public TableDataInfo list(BusAccount busAccount)
    {
        startPage();
        List<BusAccount> list = busAccountService.selectBusAccountList(busAccount);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize(hasPermi = "order:account:export")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusAccount busAccount) throws IOException
    {
        List<BusAccount> list = busAccountService.selectBusAccountList(busAccount);
        ExcelUtil<BusAccount> util = new ExcelUtil<BusAccount>(BusAccount.class);
        util.exportExcel(response, list, "订单数据");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize(hasPermi = "order:account:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busAccountService.selectBusAccountById(id));
    }

    /**
     * 新增订单
     */
//    @PreAuthorize(hasPermi = "order:account:add")
//    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BusAccount busAccount)
    {
//        return toAjax(busAccountService.insertBusAccount(busAccount));

    	return toAjax(busAccountService.updateBusAccount(busAccount));
    }

    /**
     * 修改订单
     */
    @PreAuthorize(hasPermi = "order:account:edit")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BusAccount busAccount)
    {
        return toAjax(busAccountService.updateBusAccount(busAccount));
    }

    /**
     * 删除订单
     */
    @PreAuthorize(hasPermi = "order:account:remove")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busAccountService.deleteBusAccountByIds(ids));
    }
}