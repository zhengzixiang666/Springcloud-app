package com.jdzzx.order.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdzzx.order.mapper.BusAccountMapper;
import com.jdzzx.order.domain.BusAccount;
import com.jdzzx.order.service.IBusAccountService;

/**
 * 订单Service业务层处理
 * 
 * @author zzx
 * @date 2022-02-18
 */
@Service
public class BusAccountServiceImpl implements IBusAccountService 
{
    @Autowired
    private BusAccountMapper busAccountMapper;

    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public BusAccount selectBusAccountById(Long id)
    {
        return busAccountMapper.selectBusAccountById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param busAccount 订单
     * @return 订单
     */
    @Override
    public List<BusAccount> selectBusAccountList(BusAccount busAccount)
    {
        return busAccountMapper.selectBusAccountList(busAccount);
    }

    /**
     * 新增订单
     * 
     * @param busAccount 订单
     * @return 结果
     */
    @Override
    public int insertBusAccount(BusAccount busAccount)
    {
        return busAccountMapper.insertBusAccount(busAccount);
    }

    /**
     * 修改订单
     * 
     * @param busAccount 订单
     * @return 结果
     */
    @Override
    @Transactional //单个事务生效
    public int updateBusAccount(BusAccount busAccount)
    {
        busAccountMapper.updateBusAccount(busAccount);
    	int i=1/0;//模拟报错
        return 1;
    }

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Override
    public int deleteBusAccountByIds(Long[] ids)
    {
        return busAccountMapper.deleteBusAccountByIds(ids);
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    @Override
    public int deleteBusAccountById(Long id)
    {
        return busAccountMapper.deleteBusAccountById(id);
    }
}