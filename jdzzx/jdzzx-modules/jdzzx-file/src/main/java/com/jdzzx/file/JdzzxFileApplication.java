package com.jdzzx.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import com.jdzzx.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 文件服务
 * 
 * @author zzx
 */
@EnableCustomSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class JdzzxFileApplication
{
    public static void main(String[] args)
    {
    	ConfigurableApplicationContext context =SpringApplication.run(JdzzxFileApplication.class, args);
    	Environment environment = context.getBean(Environment.class);
        System.out.println("(♥◠‿◠)ﾉﾞ  文件服务模块启动成功,端口号为"+environment.getProperty("local.server.port")+"    ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
