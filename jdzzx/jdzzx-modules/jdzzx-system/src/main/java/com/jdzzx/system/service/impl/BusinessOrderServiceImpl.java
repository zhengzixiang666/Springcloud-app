package com.jdzzx.system.service.impl;

import java.util.List;
import com.jdzzx.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jdzzx.system.mapper.BusinessOrderMapper;
import com.jdzzx.system.domain.BusinessOrder;
import com.jdzzx.system.service.IBusinessOrderService;

/**
 * 订单Service业务层处理
 * 
 * @author zzx
 * @date 2022-01-11
 */
@Service
public class BusinessOrderServiceImpl implements IBusinessOrderService 
{
    @Autowired
    private BusinessOrderMapper businessOrderMapper;

    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public BusinessOrder selectBusinessOrderById(Long id)
    {
        return businessOrderMapper.selectBusinessOrderById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param businessOrder 订单
     * @return 订单
     */
    @Override
    public List<BusinessOrder> selectBusinessOrderList(BusinessOrder businessOrder)
    {
        return businessOrderMapper.selectBusinessOrderList(businessOrder);
    }

    /**
     * 新增订单
     * 
     * @param businessOrder 订单
     * @return 结果
     */
    @Override
    public int insertBusinessOrder(BusinessOrder businessOrder)
    {
        businessOrder.setCreateTime(DateUtils.getNowDate());
        return businessOrderMapper.insertBusinessOrder(businessOrder);
    }

    /**
     * 修改订单
     * 
     * @param businessOrder 订单
     * @return 结果
     */
    @Override
    public int updateBusinessOrder(BusinessOrder businessOrder)
    {
        return businessOrderMapper.updateBusinessOrder(businessOrder);
    }

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Override
    public int deleteBusinessOrderByIds(Long[] ids)
    {
        return businessOrderMapper.deleteBusinessOrderByIds(ids);
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    @Override
    public int deleteBusinessOrderById(Long id)
    {
        return businessOrderMapper.deleteBusinessOrderById(id);
    }
}
