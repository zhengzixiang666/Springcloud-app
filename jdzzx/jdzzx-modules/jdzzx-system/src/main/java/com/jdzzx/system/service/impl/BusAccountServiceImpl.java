package com.jdzzx.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdzzx.system.mapper.BusAccountMapper;
import com.jdzzx.system.domain.BusAccount;
import com.jdzzx.system.service.IBusAccountService;

/**
 * 账单表Service业务层处理
 * 
 * @author zzx
 * @date 2022-02-18
 */
@Service
public class BusAccountServiceImpl implements IBusAccountService 
{
    @Autowired
    private BusAccountMapper busAccountMapper;

    /**
     * 查询账单表
     * 
     * @param id 账单表主键
     * @return 账单表
     */
    @Override
    public BusAccount selectBusAccountById(Long id)
    {
        return busAccountMapper.selectBusAccountById(id);
    }

    /**
     * 查询账单表列表
     * 
     * @param busAccount 账单表
     * @return 账单表
     */
    @Override
    public List<BusAccount> selectBusAccountList(BusAccount busAccount)
    {
        return busAccountMapper.selectBusAccountList(busAccount);
    }

    /**
     * 新增账单表
     * 
     * @param busAccount 账单表
     * @return 结果
     */
    @Override
    public int insertBusAccount(BusAccount busAccount)
    {
        return busAccountMapper.insertBusAccount(busAccount);
    }

    /**
     * 修改账单表
     * 
     * @param busAccount 账单表
     * @return 结果
     */
    @Override
    @Transactional //加上该注解，单个生效
    public int updateBusAccount(BusAccount busAccount)
    {
    	busAccountMapper.updateBusAccount(busAccount);
    	int i=1/0;
        return 1;
    }

    /**
     * 批量删除账单表
     * 
     * @param ids 需要删除的账单表主键
     * @return 结果
     */
    @Override
    public int deleteBusAccountByIds(Long[] ids)
    {
        return busAccountMapper.deleteBusAccountByIds(ids);
    }

    /**
     * 删除账单表信息
     * 
     * @param id 账单表主键
     * @return 结果
     */
    @Override
    public int deleteBusAccountById(Long id)
    {
        return busAccountMapper.deleteBusAccountById(id);
    }
}