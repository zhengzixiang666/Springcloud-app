package com.jdzzx.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jdzzx.common.log.annotation.Log;
import com.jdzzx.common.log.enums.BusinessType;
import com.jdzzx.common.security.annotation.PreAuthorize;
import com.jdzzx.system.domain.BusinessOrder;
import com.jdzzx.system.domain.BusinessProduct;
import com.jdzzx.system.service.IBusinessOrderService;
import com.jdzzx.system.service.IBusinessProductService;
import com.jdzzx.common.core.web.controller.BaseController;
import com.jdzzx.common.core.web.domain.AjaxResult;
import com.jdzzx.common.core.utils.CodeUtil;
import com.jdzzx.common.core.utils.bean.BeanUtils;
import com.jdzzx.common.core.utils.poi.ExcelUtil;
import com.jdzzx.common.core.web.page.TableDataInfo;

/**
 * 订单Controller
 * 
 * @author zzx
 * @date 2022-01-11
 */
@RestController
@RequestMapping("/order")
public class BusinessOrderController extends BaseController
{
    @Autowired
    private IBusinessOrderService businessOrderService;
    
    @Autowired
    private IBusinessProductService businessProductService;

    /**
     * 查询订单列表
     */
    @PreAuthorize(hasPermi = "system:order:list")
    @GetMapping("/list")
    public TableDataInfo list(BusinessOrder businessOrder)
    {
        startPage();
        List<BusinessOrder> list = businessOrderService.selectBusinessOrderList(businessOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize(hasPermi = "system:order:export")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessOrder businessOrder) throws IOException
    {
        List<BusinessOrder> list = businessOrderService.selectBusinessOrderList(businessOrder);
        ExcelUtil<BusinessOrder> util = new ExcelUtil<BusinessOrder>(BusinessOrder.class);
        util.exportExcel(response, list, "订单数据");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize(hasPermi = "system:order:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(businessOrderService.selectBusinessOrderById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize(hasPermi = "system:product:list")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BusinessOrder businessOrder)
    {
    	BusinessProduct bp=businessProductService.selectBusinessProductById(businessOrder.getProductId());
    	BusinessOrder bo=new BusinessOrder();
    	BeanUtils.copyProperties(bp, bo);
    	bo.setOrderNo(CodeUtil.getDDCode());
    	bo.setUserId(businessOrder.getUserId());
    	bo.setOrderStatus("1");
    	bo.setProductId(businessOrder.getProductId());
    	bo.setId(null);
        return toAjax(businessOrderService.insertBusinessOrder(bo));
    }

    /**
     * 修改订单
     */
    @PreAuthorize(hasPermi = "system:order:edit")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BusinessOrder businessOrder)
    {
        return toAjax(businessOrderService.updateBusinessOrder(businessOrder));
    }

    /**
     * 删除订单
     */
    @PreAuthorize(hasPermi = "system:order:remove")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(businessOrderService.deleteBusinessOrderByIds(ids));
    }
}
