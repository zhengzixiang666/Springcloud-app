package com.jdzzx.system.mapper;

import java.util.List;
import com.jdzzx.system.domain.BusinessOrder;

/**
 * 订单Mapper接口
 * 
 * @author zzx
 * @date 2022-01-11
 */
public interface BusinessOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public BusinessOrder selectBusinessOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param businessOrder 订单
     * @return 订单集合
     */
    public List<BusinessOrder> selectBusinessOrderList(BusinessOrder businessOrder);

    /**
     * 新增订单
     * 
     * @param businessOrder 订单
     * @return 结果
     */
    public int insertBusinessOrder(BusinessOrder businessOrder);

    /**
     * 修改订单
     * 
     * @param businessOrder 订单
     * @return 结果
     */
    public int updateBusinessOrder(BusinessOrder businessOrder);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteBusinessOrderById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusinessOrderByIds(Long[] ids);
}
