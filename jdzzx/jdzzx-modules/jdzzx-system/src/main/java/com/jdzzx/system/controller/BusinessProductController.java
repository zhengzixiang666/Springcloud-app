package com.jdzzx.system.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jdzzx.common.core.domain.R;
import com.jdzzx.common.core.utils.CodeUtil;
import com.jdzzx.common.core.utils.SecurityUtils;
import com.jdzzx.common.core.utils.poi.ExcelUtil;
import com.jdzzx.common.core.web.controller.BaseController;
import com.jdzzx.common.core.web.domain.AjaxResult;
import com.jdzzx.common.core.web.page.TableDataInfo;
import com.jdzzx.common.log.annotation.Log;
import com.jdzzx.common.log.enums.BusinessType;
import com.jdzzx.common.security.annotation.PreAuthorize;
import com.jdzzx.system.api.RemoteFileService;
import com.jdzzx.system.api.domain.SysFile;
import com.jdzzx.system.domain.BusinessProduct;
import com.jdzzx.system.service.IBusinessProductService;

/**
 * 版权管理Controller
 * 
 * @author zzx
 * @date 2022-01-10
 */
@RestController
@RequestMapping("/product")
public class BusinessProductController extends BaseController
{
    @Autowired
    private IBusinessProductService businessProductService;
    
    @Autowired
    private RemoteFileService remoteFileService;

    /**
     * 查询版权管理列表
     */
    @PreAuthorize(hasPermi = "system:product:list")
    @GetMapping("/list")
    public TableDataInfo list(BusinessProduct businessProduct)
    {
        startPage();
        List<BusinessProduct> list = businessProductService.selectBusinessProductList(businessProduct);
        return getDataTable(list);
    }
    
    @RequestMapping("/upload")
    public AjaxResult uploadFile(HttpServletRequest request
    		,@RequestParam(value = "id",required = false) String id
    		,@RequestParam(value = "productType",required = false) String productType
    		,@RequestParam(value = "userId",required = false) Long userId
    		,@RequestParam(value = "productName",required = false) String productName
    		,@RequestParam(value = "productRight",required = false) String productRight
    		,@RequestParam(value = "authMethod",required = false) String authMethod
    		,@RequestParam(value = "productMoney",required = false) String productMoney
    		,@RequestParam(value = "productLogo",required = false) String productLogo
    		,@RequestParam(value = "createTime",required = false) String createTime
    		,@RequestParam(value = "productNo",required = false) String productNo
    		,@RequestParam(value = "file",required = false) MultipartFile file) {
    	
    	BusinessProduct vo=new BusinessProduct();
    	vo.setApplyStatus("1");
    	vo.setProductNo(CodeUtil.getCode());
    	vo.setAuthMethod(authMethod);
    	vo.setProductMoney(productMoney);
    	vo.setProductType(productType);
    	vo.setProductName(productName);
    	vo.setProductRight(productRight);
    	R<SysFile> sysfile=remoteFileService.upload(file);
    	if(sysfile!=null && sysfile.getData()!=null) {
    		vo.setProductLogo(sysfile.getData().getUrl());
    	}
    	
    	if(id==null) {
    		if(SecurityUtils.getUserId()==null) {
    			vo.setUserId(userId);
    		}else {
    			vo.setUserId(SecurityUtils.getUserId());
    		}
    		businessProductService.insertBusinessProduct(vo);
    	}else {
    		vo.setId(Long.valueOf(id));
    		businessProductService.updateBusinessProduct(vo);
    	}
    	return   AjaxResult.success();
    }
    
    @RequestMapping("/admin/upload")
    public AjaxResult adminUpload(HttpServletRequest request
    		,@RequestParam(value = "id",required = false) String id
    		,@RequestParam(value = "productType",required = false) String productType
    		,@RequestParam(value = "userId",required = false) Long userId
    		,@RequestParam(value = "productName",required = false) String productName
    		,@RequestParam(value = "productRight",required = false) String productRight
    		,@RequestParam(value = "authMethod",required = false) String authMethod
    		,@RequestParam(value = "productMoney",required = false) String productMoney
    		,@RequestParam(value = "productLogo",required = false) String productLogo
    		,@RequestParam(value = "createTime",required = false) String createTime
    		,@RequestParam(value = "productNo",required = false) String productNo
    		,@RequestParam(value = "file",required = false) MultipartFile file) {
    	
    	BusinessProduct vo=new BusinessProduct();
    	vo.setApplyStatus("4");
    	vo.setProductNo(CodeUtil.getCode());
    	vo.setAuthMethod(authMethod);
    	vo.setProductMoney(productMoney);
    	vo.setProductType(productType);
    	vo.setProductName(productName);
    	vo.setProductRight(productRight);
    	R<SysFile> sysfile=remoteFileService.upload(file);
    	if(sysfile!=null && sysfile.getData()!=null) {
    		vo.setProductLogo(sysfile.getData().getUrl());
    	}
    	if(id==null) {
    		if(SecurityUtils.getUserId()==null) {
    			vo.setUserId(userId);
    		}else {
    			vo.setUserId(SecurityUtils.getUserId());
    		}
    		businessProductService.insertBusinessProduct(vo);
    	}else {
    		vo.setId(Long.valueOf(id));
    		businessProductService.updateBusinessProduct(vo);
    	}
    	return   AjaxResult.success();
    }

    /**
     * 导出版权管理列表
     */
    @PreAuthorize(hasPermi = "system:product:export")
    @Log(title = "版权管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessProduct businessProduct) throws IOException
    {
        List<BusinessProduct> list = businessProductService.selectBusinessProductList(businessProduct);
        ExcelUtil<BusinessProduct> util = new ExcelUtil<BusinessProduct>(BusinessProduct.class);
        util.exportExcel(response, list, "版权管理数据");
    }

    /**
     * 获取版权管理详细信息
     */
    @PreAuthorize(hasPermi = "system:product:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(businessProductService.selectBusinessProductById(id));
    }

    /**
     * 新增版权管理
     */
    @PreAuthorize(hasPermi = "system:product:add")
    @Log(title = "版权管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BusinessProduct businessProduct)
    {
    	businessProduct.setUserId(SecurityUtils.getUserId());
    	businessProduct.setApplyStatus("1");
    	businessProduct.setProductNo(CodeUtil.getCode());
        return toAjax(businessProductService.insertBusinessProduct(businessProduct));
    }

    /**
     * 修改版权管理
     */
    @PreAuthorize(hasPermi = "system:product:edit")
    @Log(title = "版权管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BusinessProduct businessProduct)
    {
        return toAjax(businessProductService.updateBusinessProduct(businessProduct));
    }

    /**
     * 删除版权管理
     */
    @PreAuthorize(hasPermi = "system:product:remove")
    @Log(title = "版权管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(businessProductService.deleteBusinessProductByIds(ids));
    }
}
