package com.jdzzx.system.service.impl;

import java.util.List;
import com.jdzzx.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jdzzx.system.mapper.BusinessNoticeMessageMapper;
import com.jdzzx.system.domain.BusinessNoticeMessage;
import com.jdzzx.system.service.IBusinessNoticeMessageService;

/**
 * 通知公告Service业务层处理
 * 
 * @author zzx
 * @date 2022-01-12
 */
@Service
public class BusinessNoticeMessageServiceImpl implements IBusinessNoticeMessageService 
{
    @Autowired
    private BusinessNoticeMessageMapper businessNoticeMessageMapper;

    /**
     * 查询通知公告
     * 
     * @param messageId 通知公告主键
     * @return 通知公告
     */
    @Override
    public BusinessNoticeMessage selectBusinessNoticeMessageByMessageId(Long messageId)
    {
        return businessNoticeMessageMapper.selectBusinessNoticeMessageByMessageId(messageId);
    }

    /**
     * 查询通知公告列表
     * 
     * @param businessNoticeMessage 通知公告
     * @return 通知公告
     */
    @Override
    public List<BusinessNoticeMessage> selectBusinessNoticeMessageList(BusinessNoticeMessage businessNoticeMessage)
    {
        return businessNoticeMessageMapper.selectBusinessNoticeMessageList(businessNoticeMessage);
    }

    /**
     * 新增通知公告
     * 
     * @param businessNoticeMessage 通知公告
     * @return 结果
     */
    @Override
    public int insertBusinessNoticeMessage(BusinessNoticeMessage businessNoticeMessage)
    {
        businessNoticeMessage.setCreateTime(DateUtils.getNowDate());
        return businessNoticeMessageMapper.insertBusinessNoticeMessage(businessNoticeMessage);
    }

    /**
     * 修改通知公告
     * 
     * @param businessNoticeMessage 通知公告
     * @return 结果
     */
    @Override
    public int updateBusinessNoticeMessage(BusinessNoticeMessage businessNoticeMessage)
    {
        return businessNoticeMessageMapper.updateBusinessNoticeMessage(businessNoticeMessage);
    }

    /**
     * 批量删除通知公告
     * 
     * @param messageIds 需要删除的通知公告主键
     * @return 结果
     */
    @Override
    public int deleteBusinessNoticeMessageByMessageIds(Long[] messageIds)
    {
        return businessNoticeMessageMapper.deleteBusinessNoticeMessageByMessageIds(messageIds);
    }

    /**
     * 删除通知公告信息
     * 
     * @param messageId 通知公告主键
     * @return 结果
     */
    @Override
    public int deleteBusinessNoticeMessageByMessageId(Long messageId)
    {
        return businessNoticeMessageMapper.deleteBusinessNoticeMessageByMessageId(messageId);
    }
}
