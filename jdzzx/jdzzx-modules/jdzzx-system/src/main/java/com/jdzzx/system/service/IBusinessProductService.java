package com.jdzzx.system.service;

import java.util.List;
import com.jdzzx.system.domain.BusinessProduct;

/**
 * 版权管理Service接口
 * 
 * @author zzx
 * @date 2022-01-10
 */
public interface IBusinessProductService 
{
    /**
     * 查询版权管理
     * 
     * @param id 版权管理主键
     * @return 版权管理
     */
    public BusinessProduct selectBusinessProductById(Long id);

    /**
     * 查询版权管理列表
     * 
     * @param businessProduct 版权管理
     * @return 版权管理集合
     */
    public List<BusinessProduct> selectBusinessProductList(BusinessProduct businessProduct);

    /**
     * 新增版权管理
     * 
     * @param businessProduct 版权管理
     * @return 结果
     */
    public int insertBusinessProduct(BusinessProduct businessProduct);

    /**
     * 修改版权管理
     * 
     * @param businessProduct 版权管理
     * @return 结果
     */
    public int updateBusinessProduct(BusinessProduct businessProduct);

    /**
     * 批量删除版权管理
     * 
     * @param ids 需要删除的版权管理主键集合
     * @return 结果
     */
    public int deleteBusinessProductByIds(Long[] ids);

    /**
     * 删除版权管理信息
     * 
     * @param id 版权管理主键
     * @return 结果
     */
    public int deleteBusinessProductById(Long id);
}
