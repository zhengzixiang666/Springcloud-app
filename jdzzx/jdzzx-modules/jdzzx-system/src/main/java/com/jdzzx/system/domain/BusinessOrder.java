package com.jdzzx.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jdzzx.common.core.annotation.Excel;
import com.jdzzx.common.core.web.domain.BaseEntity;

/**
 * 订单对象 business_order
 * 
 * @author zzx
 * @date 2022-01-11
 */
public class BusinessOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 产品类型（1.作品，2.图片，3.软件） */
    @Excel(name = "产品类型", readConverterExp = "1=.作品，2.图片，3.软件")
    private String productType;

    /** 产品图片 */
    @Excel(name = "产品图片")
    private String productLogo;

    /** 产品编号 */
    @Excel(name = "产品编号")
    private String productNo;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 版权权利（（复制权，发行权，出租权，展览权，表演权，广播权，其他权利） */
    @Excel(name = "版权权利", readConverterExp = "（=复制权，发行权，出租权，展览权，表演权，广播权，其他权利")
    private String productRight;

    /** 授权方式 */
    @Excel(name = "授权方式")
    private String authMethod;

    /** 版权价格 */
    @Excel(name = "版权价格")
    private String productMoney;

    /** 产品id */
    private Long productId;

    /** 用户id */
    private Long userId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNo;

    /** 订单状态（1，待支付，2.已完成） */
    @Excel(name = "订单状态", readConverterExp = "1=，待支付，2.已完成")
    private String orderStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductType(String productType) 
    {
        this.productType = productType;
    }

    public String getProductType() 
    {
        return productType;
    }
    public void setProductLogo(String productLogo) 
    {
        this.productLogo = productLogo;
    }

    public String getProductLogo() 
    {
        return productLogo;
    }
    public void setProductNo(String productNo) 
    {
        this.productNo = productNo;
    }

    public String getProductNo() 
    {
        return productNo;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setProductRight(String productRight) 
    {
        this.productRight = productRight;
    }

    public String getProductRight() 
    {
        return productRight;
    }
    public void setAuthMethod(String authMethod) 
    {
        this.authMethod = authMethod;
    }

    public String getAuthMethod() 
    {
        return authMethod;
    }
    public void setProductMoney(String productMoney) 
    {
        this.productMoney = productMoney;
    }

    public String getProductMoney() 
    {
        return productMoney;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setOrderStatus(String orderStatus) 
    {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus() 
    {
        return orderStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productType", getProductType())
            .append("productLogo", getProductLogo())
            .append("productNo", getProductNo())
            .append("productName", getProductName())
            .append("productRight", getProductRight())
            .append("createTime", getCreateTime())
            .append("authMethod", getAuthMethod())
            .append("productMoney", getProductMoney())
            .append("productId", getProductId())
            .append("userId", getUserId())
            .append("orderNo", getOrderNo())
            .append("orderStatus", getOrderStatus())
            .toString();
    }
}
