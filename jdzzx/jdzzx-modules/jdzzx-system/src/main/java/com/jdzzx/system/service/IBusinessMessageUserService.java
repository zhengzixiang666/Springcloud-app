package com.jdzzx.system.service;

import java.util.List;
import com.jdzzx.system.domain.BusinessMessageUser;

/**
 * 消息接收者Service接口
 * 
 * @author zzx
 * @date 2022-01-12
 */
public interface IBusinessMessageUserService 
{
    /**
     * 查询消息接收者
     * 
     * @param id 消息接收者主键
     * @return 消息接收者
     */
    public BusinessMessageUser selectBusinessMessageUserById(Long id);

    /**
     * 查询消息接收者列表
     * 
     * @param businessMessageUser 消息接收者
     * @return 消息接收者集合
     */
    public List<BusinessMessageUser> selectBusinessMessageUserList(BusinessMessageUser businessMessageUser);
    
    /**
     * 查询消息接收者列表
     * 
     * @param businessMessageUser 消息接收者
     * @return 消息接收者集合
     */
    public List<BusinessMessageUser> selectByUserId(BusinessMessageUser businessMessageUser);

    /**
     * 新增消息接收者
     * 
     * @param businessMessageUser 消息接收者
     * @return 结果
     */
    public int insertBusinessMessageUser(BusinessMessageUser businessMessageUser);

    /**
     * 修改消息接收者
     * 
     * @param businessMessageUser 消息接收者
     * @return 结果
     */
    public int updateBusinessMessageUser(BusinessMessageUser businessMessageUser);

    /**
     * 批量删除消息接收者
     * 
     * @param ids 需要删除的消息接收者主键集合
     * @return 结果
     */
    public int deleteBusinessMessageUserByIds(Long[] ids);

    /**
     * 删除消息接收者信息
     * 
     * @param id 消息接收者主键
     * @return 结果
     */
    public int deleteBusinessMessageUserById(Long id);
}
