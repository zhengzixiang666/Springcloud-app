package com.jdzzx.system.service.impl;

import java.util.List;
import com.jdzzx.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jdzzx.system.mapper.BusinessProductMapper;
import com.jdzzx.system.domain.BusinessProduct;
import com.jdzzx.system.service.IBusinessProductService;

/**
 * 版权管理Service业务层处理
 * 
 * @author zzx
 * @date 2022-01-10
 */
@Service
public class BusinessProductServiceImpl implements IBusinessProductService 
{
    @Autowired
    private BusinessProductMapper businessProductMapper;

    /**
     * 查询版权管理
     * 
     * @param id 版权管理主键
     * @return 版权管理
     */
    @Override
    public BusinessProduct selectBusinessProductById(Long id)
    {
        return businessProductMapper.selectBusinessProductById(id);
    }

    /**
     * 查询版权管理列表
     * 
     * @param businessProduct 版权管理
     * @return 版权管理
     */
    @Override
    public List<BusinessProduct> selectBusinessProductList(BusinessProduct businessProduct)
    {
        return businessProductMapper.selectBusinessProductList(businessProduct);
    }

    /**
     * 新增版权管理
     * 
     * @param businessProduct 版权管理
     * @return 结果
     */
    @Override
    public int insertBusinessProduct(BusinessProduct businessProduct)
    {
        businessProduct.setCreateTime(DateUtils.getNowDate());
        return businessProductMapper.insertBusinessProduct(businessProduct);
    }

    /**
     * 修改版权管理
     * 
     * @param businessProduct 版权管理
     * @return 结果
     */
    @Override
    public int updateBusinessProduct(BusinessProduct businessProduct)
    {
        return businessProductMapper.updateBusinessProduct(businessProduct);
    }

    /**
     * 批量删除版权管理
     * 
     * @param ids 需要删除的版权管理主键
     * @return 结果
     */
    @Override
    public int deleteBusinessProductByIds(Long[] ids)
    {
        return businessProductMapper.deleteBusinessProductByIds(ids);
    }

    /**
     * 删除版权管理信息
     * 
     * @param id 版权管理主键
     * @return 结果
     */
    @Override
    public int deleteBusinessProductById(Long id)
    {
        return businessProductMapper.deleteBusinessProductById(id);
    }
}
