package com.jdzzx.system.mapper;

import java.util.List;
import com.jdzzx.system.domain.BusAccount;

/**
 * 账单表Mapper接口
 * 
 * @author zzx
 * @date 2022-02-18
 */
public interface BusAccountMapper 
{
    /**
     * 查询账单表
     * 
     * @param id 账单表主键
     * @return 账单表
     */
    public BusAccount selectBusAccountById(Long id);

    /**
     * 查询账单表列表
     * 
     * @param busAccount 账单表
     * @return 账单表集合
     */
    public List<BusAccount> selectBusAccountList(BusAccount busAccount);

    /**
     * 新增账单表
     * 
     * @param busAccount 账单表
     * @return 结果
     */
    public int insertBusAccount(BusAccount busAccount);

    /**
     * 修改账单表
     * 
     * @param busAccount 账单表
     * @return 结果
     */
    public int updateBusAccount(BusAccount busAccount);

    /**
     * 删除账单表
     * 
     * @param id 账单表主键
     * @return 结果
     */
    public int deleteBusAccountById(Long id);

    /**
     * 批量删除账单表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusAccountByIds(Long[] ids);
}