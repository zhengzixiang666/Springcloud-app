package com.jdzzx.system.mapper;

import java.util.List;
import com.jdzzx.system.domain.BusinessNoticeMessage;

/**
 * 通知公告Mapper接口
 * 
 * @author zzx
 * @date 2022-01-12
 */
public interface BusinessNoticeMessageMapper 
{
    /**
     * 查询通知公告
     * 
     * @param messageId 通知公告主键
     * @return 通知公告
     */
    public BusinessNoticeMessage selectBusinessNoticeMessageByMessageId(Long messageId);

    /**
     * 查询通知公告列表
     * 
     * @param businessNoticeMessage 通知公告
     * @return 通知公告集合
     */
    public List<BusinessNoticeMessage> selectBusinessNoticeMessageList(BusinessNoticeMessage businessNoticeMessage);

    /**
     * 新增通知公告
     * 
     * @param businessNoticeMessage 通知公告
     * @return 结果
     */
    public int insertBusinessNoticeMessage(BusinessNoticeMessage businessNoticeMessage);

    /**
     * 修改通知公告
     * 
     * @param businessNoticeMessage 通知公告
     * @return 结果
     */
    public int updateBusinessNoticeMessage(BusinessNoticeMessage businessNoticeMessage);

    /**
     * 删除通知公告
     * 
     * @param messageId 通知公告主键
     * @return 结果
     */
    public int deleteBusinessNoticeMessageByMessageId(Long messageId);

    /**
     * 批量删除通知公告
     * 
     * @param messageIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusinessNoticeMessageByMessageIds(Long[] messageIds);
}
