package com.jdzzx.system.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jdzzx.common.core.annotation.Excel;
import com.jdzzx.common.core.web.domain.BaseEntity;

/**
 * 消息发布对象 business_notice_message
 * 
 * @author zzx
 * @date 2022-01-12
 */
public class BusinessNoticeMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 消息ID */
    private Long messageId;

    /** 消息标题 */
    @Excel(name = "消息标题")
    private String messageTitle;

    /** 消息内容 */
    private String messageContent;

    /** 消息状态（1.未发送，2.已发送） */
    @Excel(name = "消息状态", readConverterExp = "1=.未发送，2.已发送")
    private String messageStatus;
    
    /**
     * 用户ids
     */
    private List<Long> userIds;
    
    

    public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	public void setMessageId(Long messageId) 
    {
        this.messageId = messageId;
    }

    public Long getMessageId() 
    {
        return messageId;
    }
    public void setMessageTitle(String messageTitle) 
    {
        this.messageTitle = messageTitle;
    }

    public String getMessageTitle() 
    {
        return messageTitle;
    }
    public void setMessageContent(String messageContent) 
    {
        this.messageContent = messageContent;
    }

    public String getMessageContent() 
    {
        return messageContent;
    }
    public void setMessageStatus(String messageStatus) 
    {
        this.messageStatus = messageStatus;
    }

    public String getMessageStatus() 
    {
        return messageStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("messageId", getMessageId())
            .append("messageTitle", getMessageTitle())
            .append("messageContent", getMessageContent())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("messageStatus", getMessageStatus())
            .toString();
    }
}
