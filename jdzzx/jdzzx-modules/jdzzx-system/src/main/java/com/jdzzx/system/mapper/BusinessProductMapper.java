package com.jdzzx.system.mapper;

import java.util.List;
import com.jdzzx.system.domain.BusinessProduct;

/**
 * 版权管理Mapper接口
 * 
 * @author zzx
 * @date 2022-01-10
 */
public interface BusinessProductMapper 
{
    /**
     * 查询版权管理
     * 
     * @param id 版权管理主键
     * @return 版权管理
     */
    public BusinessProduct selectBusinessProductById(Long id);

    /**
     * 查询版权管理列表
     * 
     * @param businessProduct 版权管理
     * @return 版权管理集合
     */
    public List<BusinessProduct> selectBusinessProductList(BusinessProduct businessProduct);

    /**
     * 新增版权管理
     * 
     * @param businessProduct 版权管理
     * @return 结果
     */
    public int insertBusinessProduct(BusinessProduct businessProduct);

    /**
     * 修改版权管理
     * 
     * @param businessProduct 版权管理
     * @return 结果
     */
    public int updateBusinessProduct(BusinessProduct businessProduct);

    /**
     * 删除版权管理
     * 
     * @param id 版权管理主键
     * @return 结果
     */
    public int deleteBusinessProductById(Long id);

    /**
     * 批量删除版权管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusinessProductByIds(Long[] ids);
}
