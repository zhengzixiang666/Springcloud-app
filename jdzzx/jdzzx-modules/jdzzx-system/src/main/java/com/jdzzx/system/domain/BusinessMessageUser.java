package com.jdzzx.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.jdzzx.common.core.annotation.Excel;
import com.jdzzx.common.core.web.domain.BaseEntity;

/**
 * 消息用户对象 business_message_user
 * 
 * @author zzx
 * @date 2022-01-12
 */
public class BusinessMessageUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 消息发送id */
    private Long id;

    /** 消息id */
    @Excel(name = "消息id")
    private Long messageId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 消息状态(1.未读。2.已读) */
    @Excel(name = "消息状态(1.未读。2.已读)")
    private String redonlyStatus;
    
    /** 消息标题 */
    @Excel(name = "消息标题")
    private String messageTitle;

    /** 消息内容 */
    private String messageContent;
    
    

    public String getMessageTitle() {
		return messageTitle;
	}

	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMessageId(Long messageId) 
    {
        this.messageId = messageId;
    }

    public Long getMessageId() 
    {
        return messageId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setRedonlyStatus(String redonlyStatus) 
    {
        this.redonlyStatus = redonlyStatus;
    }

    public String getRedonlyStatus() 
    {
        return redonlyStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("messageId", getMessageId())
            .append("userId", getUserId())
            .append("redonlyStatus", getRedonlyStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}