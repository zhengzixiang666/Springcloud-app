package com.jdzzx.system.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jdzzx.common.core.utils.poi.ExcelUtil;
import com.jdzzx.common.core.web.controller.BaseController;
import com.jdzzx.common.core.web.domain.AjaxResult;
import com.jdzzx.common.core.web.page.TableDataInfo;
import com.jdzzx.common.log.annotation.Log;
import com.jdzzx.common.log.enums.BusinessType;
import com.jdzzx.common.security.annotation.PreAuthorize;
import com.jdzzx.system.api.RemoteOrderService;
import com.jdzzx.system.api.domain.BusAccountApi;
import com.jdzzx.system.domain.BusAccount;
import com.jdzzx.system.service.IBusAccountService;

import io.seata.spring.annotation.GlobalTransactional;

/**
 * 该类为测试分布式事务所用
 * 
 * @author zzx
 * @date 2022-02-18
 */
@RestController
@RequestMapping("/account")
public class BusAccountController extends BaseController
{
    @Autowired
    private IBusAccountService busAccountService;
    
    @Autowired
    private RemoteOrderService remoteOrderService;

    /**
     * 查询账单表列表
     */
    @PreAuthorize(hasPermi = "system:account:list")
    @GetMapping("/list")
    public TableDataInfo list(BusAccount busAccount)
    {
        startPage();
        List<BusAccount> list = busAccountService.selectBusAccountList(busAccount);
        return getDataTable(list);
    }

    /**
     * 导出账单表列表
     */
    @PreAuthorize(hasPermi = "system:account:export")
    @Log(title = "账单表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusAccount busAccount) throws IOException
    {
        List<BusAccount> list = busAccountService.selectBusAccountList(busAccount);
        ExcelUtil<BusAccount> util = new ExcelUtil<BusAccount>(BusAccount.class);
        util.exportExcel(response, list, "账单表数据");
    }

    /**
     * 获取账单表详细信息
     */
    @PreAuthorize(hasPermi = "system:account:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busAccountService.selectBusAccountById(id));
    }

    /**
     * 新增账单表
     */
//    @PreAuthorize(hasPermi = "system:account:add")
//    @Log(title = "账单表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @GlobalTransactional //测试分布式事务 已生效
    public AjaxResult add(@RequestBody(required = false) BusAccount busAccount)
    {
    	//减去李四的钱
    	BusAccount a=new BusAccount();
    	a.setId(1L);
    	a.setBalance(500L);
    	busAccountService.updateBusAccount(a);
    	//验证system模块事务
    	
    	//给王六加钱
    	BusAccountApi ba=new BusAccountApi();
    	ba.setId(1L);
    	ba.setBalance(8000L);
    	remoteOrderService.add(ba);
        return AjaxResult.success("执行成功");
    }

    /**
     * 修改账单表
     */
    @PreAuthorize(hasPermi = "system:account:edit")
    @Log(title = "账单表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BusAccount busAccount)
    {
        return toAjax(busAccountService.updateBusAccount(busAccount));
    }

    /**
     * 删除账单表
     */
    @PreAuthorize(hasPermi = "system:account:remove")
    @Log(title = "账单表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busAccountService.deleteBusAccountByIds(ids));
    }
}