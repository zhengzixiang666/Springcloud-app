package com.jdzzx.system.service.impl;

import java.util.List;
import com.jdzzx.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jdzzx.system.mapper.BusinessMessageUserMapper;
import com.jdzzx.system.domain.BusinessMessageUser;
import com.jdzzx.system.service.IBusinessMessageUserService;

/**
 * 消息接收者Service业务层处理
 * 
 * @author zzx
 * @date 2022-01-12
 */
@Service
public class BusinessMessageUserServiceImpl implements IBusinessMessageUserService 
{
    @Autowired
    private BusinessMessageUserMapper businessMessageUserMapper;

    /**
     * 查询消息接收者
     * 
     * @param id 消息接收者主键
     * @return 消息接收者
     */
    @Override
    public BusinessMessageUser selectBusinessMessageUserById(Long id)
    {
        return businessMessageUserMapper.selectBusinessMessageUserById(id);
    }

    /**
     * 查询消息接收者列表
     * 
     * @param businessMessageUser 消息接收者
     * @return 消息接收者
     */
    @Override
    public List<BusinessMessageUser> selectBusinessMessageUserList(BusinessMessageUser businessMessageUser)
    {
        return businessMessageUserMapper.selectBusinessMessageUserList(businessMessageUser);
    }

    /**
     * 新增消息接收者
     * 
     * @param businessMessageUser 消息接收者
     * @return 结果
     */
    @Override
    public int insertBusinessMessageUser(BusinessMessageUser businessMessageUser)
    {
        businessMessageUser.setCreateTime(DateUtils.getNowDate());
        return businessMessageUserMapper.insertBusinessMessageUser(businessMessageUser);
    }

    /**
     * 修改消息接收者
     * 
     * @param businessMessageUser 消息接收者
     * @return 结果
     */
    @Override
    public int updateBusinessMessageUser(BusinessMessageUser businessMessageUser)
    {
        businessMessageUser.setUpdateTime(DateUtils.getNowDate());
        return businessMessageUserMapper.updateBusinessMessageUser(businessMessageUser);
    }

    /**
     * 批量删除消息接收者
     * 
     * @param ids 需要删除的消息接收者主键
     * @return 结果
     */
    @Override
    public int deleteBusinessMessageUserByIds(Long[] ids)
    {
        return businessMessageUserMapper.deleteBusinessMessageUserByIds(ids);
    }

    /**
     * 删除消息接收者信息
     * 
     * @param id 消息接收者主键
     * @return 结果
     */
    @Override
    public int deleteBusinessMessageUserById(Long id)
    {
        return businessMessageUserMapper.deleteBusinessMessageUserById(id);
    }

	@Override
	public List<BusinessMessageUser> selectByUserId(BusinessMessageUser businessMessageUser) {
		return businessMessageUserMapper.selectByUserId(businessMessageUser);
	}
}
