package com.jdzzx.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jdzzx.common.log.annotation.Log;
import com.jdzzx.common.log.enums.BusinessType;
import com.jdzzx.common.security.annotation.PreAuthorize;
import com.jdzzx.system.domain.BusinessMessageUser;
import com.jdzzx.system.service.IBusinessMessageUserService;
import com.jdzzx.common.core.web.controller.BaseController;
import com.jdzzx.common.core.web.domain.AjaxResult;
import com.jdzzx.common.core.utils.SecurityUtils;
import com.jdzzx.common.core.utils.poi.ExcelUtil;
import com.jdzzx.common.core.web.page.TableDataInfo;

/**
 * 消息接收者Controller
 * 
 * @author zzx
 * @date 2022-01-12
 */
@RestController
@RequestMapping("/messageuser")
public class BusinessMessageUserController extends BaseController
{
    @Autowired
    private IBusinessMessageUserService businessMessageUserService;

    /**
     * 查询消息接收者列表
     */
    @PreAuthorize(hasPermi = "system:messageuser:list")
    @GetMapping("/list")
    public TableDataInfo list(BusinessMessageUser businessMessageUser)
    {
        startPage();
        List<BusinessMessageUser> list = businessMessageUserService.selectBusinessMessageUserList(businessMessageUser);
        return getDataTable(list);
    }
    
    /**
     * 查询消息接收者列表
     */
    @PreAuthorize(hasPermi = "system:message:list")
    @GetMapping("/selectByUserId")
    public TableDataInfo selectByUserId(BusinessMessageUser businessMessageUser)
    {
    	businessMessageUser.setUserId(SecurityUtils.getUserId());
        startPage();
        System.out.println(businessMessageUser.getRedonlyStatus());
        List<BusinessMessageUser> list = businessMessageUserService.selectByUserId(businessMessageUser);
        return getDataTable(list);
    }

    /**
     * 导出消息接收者列表
     */
    @PreAuthorize(hasPermi = "system:messageuser:export")
    @Log(title = "消息接收者", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessMessageUser businessMessageUser) throws IOException
    {
        List<BusinessMessageUser> list = businessMessageUserService.selectBusinessMessageUserList(businessMessageUser);
        ExcelUtil<BusinessMessageUser> util = new ExcelUtil<BusinessMessageUser>(BusinessMessageUser.class);
        util.exportExcel(response, list, "消息接收者数据");
    }

    /**
     * 获取消息接收者详细信息
     */
    @PreAuthorize(hasPermi = "system:messageuser:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(businessMessageUserService.selectBusinessMessageUserById(id));
    }

    /**
     * 新增消息接收者
     */
    @PreAuthorize(hasPermi = "system:messageuser:add")
    @Log(title = "消息接收者", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BusinessMessageUser businessMessageUser)
    {
        return toAjax(businessMessageUserService.insertBusinessMessageUser(businessMessageUser));
    }

    /**
     * 修改消息接收者
     */
    @PreAuthorize(hasPermi = "system:messageuser:edit")
    @Log(title = "消息接收者", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BusinessMessageUser businessMessageUser)
    {
        return toAjax(businessMessageUserService.updateBusinessMessageUser(businessMessageUser));
    }

    /**
     * 删除消息接收者
     */
    @PreAuthorize(hasPermi = "system:messageuser:remove")
    @Log(title = "消息接收者", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(businessMessageUserService.deleteBusinessMessageUserByIds(ids));
    }
}
