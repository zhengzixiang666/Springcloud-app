package com.jdzzx.system.service;

import java.util.List;
import com.jdzzx.system.domain.BusinessNoticeMessage;

/**
 * 通知公告Service接口
 * 
 * @author zzx
 * @date 2022-01-12
 */
public interface IBusinessNoticeMessageService 
{
    /**
     * 查询通知公告
     * 
     * @param messageId 通知公告主键
     * @return 通知公告
     */
    public BusinessNoticeMessage selectBusinessNoticeMessageByMessageId(Long messageId);

    /**
     * 查询通知公告列表
     * 
     * @param businessNoticeMessage 通知公告
     * @return 通知公告集合
     */
    public List<BusinessNoticeMessage> selectBusinessNoticeMessageList(BusinessNoticeMessage businessNoticeMessage);

    /**
     * 新增通知公告
     * 
     * @param businessNoticeMessage 通知公告
     * @return 结果
     */
    public int insertBusinessNoticeMessage(BusinessNoticeMessage businessNoticeMessage);

    /**
     * 修改通知公告
     * 
     * @param businessNoticeMessage 通知公告
     * @return 结果
     */
    public int updateBusinessNoticeMessage(BusinessNoticeMessage businessNoticeMessage);

    /**
     * 批量删除通知公告
     * 
     * @param messageIds 需要删除的通知公告主键集合
     * @return 结果
     */
    public int deleteBusinessNoticeMessageByMessageIds(Long[] messageIds);

    /**
     * 删除通知公告信息
     * 
     * @param messageId 通知公告主键
     * @return 结果
     */
    public int deleteBusinessNoticeMessageByMessageId(Long messageId);
}
