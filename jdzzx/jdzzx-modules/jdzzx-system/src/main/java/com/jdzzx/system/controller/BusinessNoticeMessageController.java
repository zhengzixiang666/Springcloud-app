package com.jdzzx.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jdzzx.common.log.annotation.Log;
import com.jdzzx.common.log.enums.BusinessType;
import com.jdzzx.common.security.annotation.PreAuthorize;
import com.jdzzx.system.domain.BusinessMessageUser;
import com.jdzzx.system.domain.BusinessNoticeMessage;
import com.jdzzx.system.service.IBusinessMessageUserService;
import com.jdzzx.system.service.IBusinessNoticeMessageService;
import com.jdzzx.common.core.web.controller.BaseController;
import com.jdzzx.common.core.web.domain.AjaxResult;
import com.jdzzx.common.core.utils.poi.ExcelUtil;
import com.jdzzx.common.core.web.page.TableDataInfo;

/**
 * 消息发布Controller
 * 
 * @author zzx
 * @date 2022-01-12
 */
@RestController
@RequestMapping("/message")
public class BusinessNoticeMessageController extends BaseController
{
    @Autowired
    private IBusinessNoticeMessageService businessNoticeMessageService;
    
    @Autowired
    private IBusinessMessageUserService businessMessageUserService;

    /**
     * 查询消息发布列表
     */
    @PreAuthorize(hasPermi = "system:message:list")
    @GetMapping("/list")
    public TableDataInfo list(BusinessNoticeMessage businessNoticeMessage)
    {
        startPage();
        List<BusinessNoticeMessage> list = businessNoticeMessageService.selectBusinessNoticeMessageList(businessNoticeMessage);
        return getDataTable(list);
    }

    /**
     * 导出消息发布列表
     */
    @PreAuthorize(hasPermi = "system:message:export")
    @Log(title = "消息发布", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessNoticeMessage businessNoticeMessage) throws IOException
    {
        List<BusinessNoticeMessage> list = businessNoticeMessageService.selectBusinessNoticeMessageList(businessNoticeMessage);
        ExcelUtil<BusinessNoticeMessage> util = new ExcelUtil<BusinessNoticeMessage>(BusinessNoticeMessage.class);
        util.exportExcel(response, list, "消息发布数据");
    }

    /**
     * 获取消息发布详细信息
     */
    @PreAuthorize(hasPermi = "system:message:query")
    @GetMapping(value = "/{messageId}")
    public AjaxResult getInfo(@PathVariable("messageId") Long messageId)
    {
        return AjaxResult.success(businessNoticeMessageService.selectBusinessNoticeMessageByMessageId(messageId));
    }

    /**
     * 新增消息发布
     */
    @PreAuthorize(hasPermi = "system:message:add")
    @Log(title = "消息发布", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BusinessNoticeMessage businessNoticeMessage)
    {
    	businessNoticeMessage.setMessageStatus("1");
        return toAjax(businessNoticeMessageService.insertBusinessNoticeMessage(businessNoticeMessage));
    }
    
    /**
     * 发送消息
     */
    @PreAuthorize(hasPermi = "system:message:add")
    @Log(title = "消息发布", businessType = BusinessType.INSERT)
    @PostMapping("/sendMessage")
    public AjaxResult sendMessage(@RequestBody BusinessNoticeMessage businessNoticeMessage)
    {
    	List<Long> list=businessNoticeMessage.getUserIds();
    	if(!CollectionUtils.isEmpty(list)) {
    		for (int i = 0; i < list.size(); i++) {
    			BusinessMessageUser businessMessageUser=new BusinessMessageUser();
    			businessMessageUser.setMessageId(businessNoticeMessage.getMessageId());
    			businessMessageUser.setUserId(list.get(i));
    			businessMessageUser.setRedonlyStatus("1");
    			businessMessageUserService.insertBusinessMessageUser(businessMessageUser);
			}
    	}
    	BusinessNoticeMessage bn=new BusinessNoticeMessage();
    	bn.setMessageStatus("2");
    	bn.setMessageId(businessNoticeMessage.getMessageId());
        return toAjax(businessNoticeMessageService.updateBusinessNoticeMessage(bn));
    }

    /**
     * 修改消息发布
     */
    @PreAuthorize(hasPermi = "system:message:edit")
    @Log(title = "消息发布", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BusinessNoticeMessage businessNoticeMessage)
    {
        return toAjax(businessNoticeMessageService.updateBusinessNoticeMessage(businessNoticeMessage));
    }

    /**
     * 删除消息发布
     */
    @PreAuthorize(hasPermi = "system:message:remove")
    @Log(title = "消息发布", businessType = BusinessType.DELETE)
	@DeleteMapping("/{messageIds}")
    public AjaxResult remove(@PathVariable Long[] messageIds)
    {
        return toAjax(businessNoticeMessageService.deleteBusinessNoticeMessageByMessageIds(messageIds));
    }
}
