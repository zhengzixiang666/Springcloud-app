package com.jdzzx.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import com.jdzzx.common.security.annotation.EnableRyFeignClients;

/**
 * 认证授权中心
 * 
 * @author zzx
 */
@EnableRyFeignClients
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class JdzzxAuthApplication
{
    public static void main(String[] args)
    {
    	ConfigurableApplicationContext context =SpringApplication.run(JdzzxAuthApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功,端口号为"+environment.getProperty("local.server.port")+"   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
