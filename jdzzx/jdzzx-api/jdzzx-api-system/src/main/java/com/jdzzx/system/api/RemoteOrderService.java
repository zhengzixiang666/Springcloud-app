package com.jdzzx.system.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.jdzzx.common.core.constant.ServiceNameConstants;
import com.jdzzx.common.core.domain.R;
import com.jdzzx.system.api.domain.BusAccountApi;
import com.jdzzx.system.api.domain.SysFile;
import com.jdzzx.system.api.factory.RemoteOrderFallbackFactory;

/**
 * 文件服务
 * 
 * @author zzx
 */
@FeignClient(contextId = "remoteOrderService", value = ServiceNameConstants.ORDER_SERVICE, fallbackFactory = RemoteOrderFallbackFactory.class)
public interface RemoteOrderService
{
    /**
     * 上传文件
     *
     * @param file 文件信息
     * @return 结果
     */
    @PostMapping(value = "/account/add")
    public R<SysFile> add(@RequestBody BusAccountApi busAccount);
}
