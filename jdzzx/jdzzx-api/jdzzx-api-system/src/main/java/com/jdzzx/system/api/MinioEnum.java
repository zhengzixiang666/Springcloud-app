package com.jdzzx.system.api;

public enum MinioEnum {

	
    MINIO_PRODUCT("product", "版权产品"),
    ;

    private final String name;
    private final String remark;

    MinioEnum(String name, String remark) {
        this.name = name;
        this.remark = remark;
    }

    public String getName() {
        return name;
    }

    public String getRemark() {
        return remark;
    }
}
