package com.jdzzx.system.api.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import com.jdzzx.common.core.domain.R;
import com.jdzzx.system.api.RemoteOrderService;
import com.jdzzx.system.api.domain.BusAccountApi;
import com.jdzzx.system.api.domain.SysFile;
@Component
public class RemoteOrderFallbackFactory implements FallbackFactory<RemoteOrderService>{
	
	private static final Logger log = LoggerFactory.getLogger(RemoteOrderFallbackFactory.class);

    @Override
    public RemoteOrderService create(Throwable throwable)
    {
        log.error("文件服务调用失败:{}", throwable.getMessage());
        return new RemoteOrderService()
        {
			@Override
			public R<SysFile> add(BusAccountApi busAccount) {
				// TODO Auto-generated method stub
			    return R.fail("执行失败:" + throwable.getMessage());
			}
        };
    }

}
