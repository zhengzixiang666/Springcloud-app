package com.jdzzx.common.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DateTimeUtil {
	
	public DateTimeUtil(){}
	
	//将时间转为有固定格式的字符串
	public static String formatDate(String format,Date date){
		if(date==null){
			return null;
		}
		return new SimpleDateFormat(format).format(date);
	};
	
	//将字符串转为有格式时间
	public static Date parseDate(String str,String format,Date date){
		if(StringUtils.isEmpty(str)){
			return date;
		}
		try {
			return new SimpleDateFormat(format).parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return date;
		}
	};
	//将日期转为有格式的日期
	public static Date formatDateToDate(String format,Date date){
		if(date==null){
			return null;
		}
		try {
			return new SimpleDateFormat(format).parse(date.toString());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	};
	
	//获取当前日期
	public static Date getCurrentTime(){
		return new Date();
	};

}
