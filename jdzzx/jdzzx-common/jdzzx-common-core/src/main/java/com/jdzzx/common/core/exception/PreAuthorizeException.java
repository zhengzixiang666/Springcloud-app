package com.jdzzx.common.core.exception;

/**
 * 权限异常
 * 
 * @author zzx
 */
public class PreAuthorizeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException()
    {
    }
}
