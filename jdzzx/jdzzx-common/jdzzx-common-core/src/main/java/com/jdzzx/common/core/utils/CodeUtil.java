package com.jdzzx.common.core.utils;

import java.util.Date;

public class CodeUtil {
	
	private static final String DEFAULT="CP";
	
	private static final String FILE="";
	
	public static String getCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMddHHmmss", date);
		return DEFAULT+code;
	}
	
	public static String getFileCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMdd", date);
		return FILE+code;
	}

	public static void main(String[] args) {
		System.out.println(CodeUtil.getCode());
	}
	
	public static String getDDCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMddHHmmss", date);
		return "DD"+code;
	}
	
	public static String getYGCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMddHHmmss", date);
		return "E"+code;
	}
	
	public static String getQJCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMddHHmmss", date);
		return "QJ"+code;
	}
	
	public static String getBXCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMddHHmmss", date);
		return "BX"+code;
	}
}
