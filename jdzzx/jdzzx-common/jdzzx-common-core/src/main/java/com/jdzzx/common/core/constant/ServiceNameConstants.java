package com.jdzzx.common.core.constant;

/**
 * 服务名称
 * 
 * @author zzx
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "jdzzx-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "jdzzx-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "jdzzx-file";
    
    /**
     * 订单服务的serviceid
     */
    public static final String ORDER_SERVICE = "jdzzx-order";
}
