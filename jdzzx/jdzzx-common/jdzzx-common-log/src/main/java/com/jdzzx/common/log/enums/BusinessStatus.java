package com.jdzzx.common.log.enums;

/**
 * 操作状态
 * 
 * @author zzx
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
